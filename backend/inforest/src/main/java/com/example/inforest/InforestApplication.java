package com.example.inforest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
		//(exclude = DataSourceAutoConfiguration.class)
public class InforestApplication {

	public static void main(String[] args) {
		SpringApplication.run(InforestApplication.class, args);
	}

}
