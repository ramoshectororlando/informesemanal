package com.example.inforest.service;

import com.example.inforest.model.Region;

import java.util.List;

public interface RegionService {

    List<Region> getAll();
    Region get(int id);
    Region save(Region region);
    Region update(Region region);
    void delete(int id);
    void delete(Region region);
}
