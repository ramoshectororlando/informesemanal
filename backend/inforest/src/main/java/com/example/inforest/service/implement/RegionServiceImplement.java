package com.example.inforest.service.implement;

import com.example.inforest.model.Region;
import com.example.inforest.repository.RegionRepository;
import com.example.inforest.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionServiceImplement implements RegionService {

    @Autowired
    private RegionRepository regionRepository;

    @Override
    public List<Region> getAll() {
        return regionRepository.findAll();
    }

    @Override
    public Region get(int id) {
        return regionRepository.findById(id).get();
    }

    @Override
    public Region save(Region region) {
        return regionRepository.save(region);
    }

    @Override
    public Region update(Region region) {
        return regionRepository.save(region);
    }

    @Override
    public void delete(int id) {
        regionRepository.deleteById(id);
    }

    @Override
    public void delete(Region region) {
        regionRepository.delete(region);
    }
}
