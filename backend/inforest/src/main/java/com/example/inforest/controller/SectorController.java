package com.example.inforest.controller;

import com.example.inforest.model.Domicilio;
import com.example.inforest.model.Sector;
import com.example.inforest.service.SectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sector")
public class SectorController {

    @Autowired
    SectorService sectorService;

    @GetMapping
    public List<Sector> getAll(){
        return sectorService.getAll();
    }

    @GetMapping(params = {"id"})
    public Sector get(@RequestParam int id){
        return  sectorService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Sector save(@RequestBody Sector sector){
        return  sectorService.save(sector);
    }

    @PutMapping(consumes = {"application/json"})
    public  Sector update(@RequestBody Sector sector){
        if (sector.getId()==null);
        return sectorService.update(sector);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        sectorService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Sector sector){
        sectorService.delete(sector);
    }
}
