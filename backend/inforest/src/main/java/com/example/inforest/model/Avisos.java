package com.example.inforest.model;

import javax.persistence.*;

@Entity
@Table(name = "avisos")
public class Avisos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_aviso", length = 10)
    private Integer id;

    @Column(name = "titulo", length = 50)
    private String titulo;

    @Column(name = "mensaje", length = 2000)
    private String mensaje;

    @Column(name = "fecha", length = 30)
    private String fecha;

    @Column(name = "eliminado", length = 5)
    private Boolean eliminado;

    @Column(name = "leido", length = 5)
    private Boolean leido;

    public Avisos() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Boolean getEliminado() {
        return eliminado;
    }

    public void setEliminado(Boolean eliminado) {
        this.eliminado = eliminado;
    }

    public Boolean getLeido() {
        return leido;
    }

    public void setLeido(Boolean leido) {
        this.leido = leido;
    }
}
