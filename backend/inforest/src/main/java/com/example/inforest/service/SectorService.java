package com.example.inforest.service;

import com.example.inforest.model.Domicilio;
import com.example.inforest.model.Sector;

import java.util.List;

public interface SectorService {

    List<Sector> getAll();
    Sector get(int id);
    Sector save(Sector sector);
    Sector update(Sector sector);
    void delete(int id);
    void delete(Sector sector);
}
