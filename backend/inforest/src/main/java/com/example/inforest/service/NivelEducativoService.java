package com.example.inforest.service;

import com.example.inforest.model.Domicilio;
import com.example.inforest.model.NivelEducativo;

import java.util.List;

public interface NivelEducativoService {

    List<NivelEducativo> getAll();
    NivelEducativo get(int id);
    NivelEducativo save(NivelEducativo nivelEducativo);
    NivelEducativo update(NivelEducativo nivelEducativo);
    void delete(int id);
    void delete(NivelEducativo nivelEducativo);
}
