package com.example.inforest.repository;

import com.example.inforest.model.Entidad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntidadRepository extends JpaRepository<Entidad, Integer> {
}
