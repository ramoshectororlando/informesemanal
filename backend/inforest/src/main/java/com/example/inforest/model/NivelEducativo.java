package com.example.inforest.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "nivel_educativo")
public class NivelEducativo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nivel", length = 10)
    private Integer id;

    @Column(name = "nivel", length = 30)
    private String nivel;

//    @JsonBackReference
    @JsonIgnore
//    @OneToMany(targetEntity = Escuela.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "nivelEducativo")
@OneToMany(mappedBy = "nivelEducativo")
private List<Escuela> escuelas;

    public NivelEducativo() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public List<Escuela> getEscuelas() {
        return escuelas;
    }

    public void setEscuelas(List<Escuela> escuelas) {
        this.escuelas = escuelas;
    }
}
