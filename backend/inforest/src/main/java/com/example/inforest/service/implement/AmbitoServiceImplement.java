package com.example.inforest.service.implement;

import com.example.inforest.model.Ambito;
import com.example.inforest.repository.AmbitoRepository;
import com.example.inforest.service.AmbitoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AmbitoServiceImplement implements AmbitoService {

    @Autowired
    private AmbitoRepository ambitoRepository;

    @Override
    public List<Ambito> getAll() {
        return ambitoRepository.findAll();
    }

    @Override
    public Ambito get(int id) {
        return ambitoRepository.findById(id).get();
    }

    @Override
    public Ambito save(Ambito ambito) {
        return ambitoRepository.save(ambito);
    }

    @Override
    public Ambito update(Ambito ambito) {
        return ambitoRepository.save(ambito);
    }

    @Override
    public void delete(int id) {
        ambitoRepository.deleteById(id);
    }

    @Override
    public void delete(Ambito ambito) {
        ambitoRepository.delete(ambito);
    }
}
