package com.example.inforest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "categoria")
public class Categoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_categoria", length = 10)
    private Integer id;

    @Column(name = "categoria",length = 100)
    private String categoria;

    @ManyToOne
    @JoinColumn(name = "id_categoria_categoria")
    private CategoriaCategoria categoriaCategoria;

    @JsonIgnore
//    @OneToMany(targetEntity = Actividad.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "categoria")
    @OneToMany(mappedBy = "categoria")
    private List<Actividad> actividadList;

    public Categoria() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public List<Actividad> getActividadList() {
        return actividadList;
    }

    public void setActividadList(List<Actividad> actividadList) {
        this.actividadList = actividadList;
    }

    public CategoriaCategoria getCategoriaCategoria() {
        return categoriaCategoria;
    }

    public void setCategoriaCategoria(CategoriaCategoria categoriaCategoria) {
        this.categoriaCategoria = categoriaCategoria;
    }

}
