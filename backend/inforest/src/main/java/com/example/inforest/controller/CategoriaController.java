package com.example.inforest.controller;

import com.example.inforest.model.*;
import com.example.inforest.service.CategoriaCategoriaService;
import com.example.inforest.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categoria")
public class CategoriaController {

    @Autowired
    CategoriaService categoriaService;
    @Autowired
    CategoriaCategoriaService categoriaCategoriaService;

    @GetMapping
    public List<Categoria> getAll(){
        return categoriaService.getAll();
    }

    @GetMapping(params = {"id"})
    public Categoria get(@RequestParam int id){
        return categoriaService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Categoria save(@RequestBody Categoria categoria){
        return  categoriaService.save(verificarSave(categoria));
    }

    @PutMapping(consumes = {"application/json"})
    public  Categoria update(@RequestBody Categoria categoria){
        if (categoria.getId()==null);
        return categoriaService.update(categoria);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        categoriaService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Categoria categoria){
        categoriaService.delete(categoria);
    }

    private Categoria verificarSave(Categoria categoria){
        CategoriaCategoria categoriaCategoria = null;
        if(categoria.getCategoriaCategoria().getId() == null){
            categoriaCategoria = categoriaCategoriaService.save(categoria.getCategoriaCategoria());
        }
        return categoria;
    }
}
