package com.example.inforest.controller;

import com.example.inforest.model.Ambito;
import com.example.inforest.service.AmbitoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ambito")
public class AmbitoController {

    @Autowired
    AmbitoService ambitoService;

    @GetMapping
    public List<Ambito> getAll(){
        return ambitoService.getAll();
    }

    @GetMapping(params = {"id"})
    public Ambito get(@RequestParam int id){
        return  ambitoService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Ambito save(@RequestBody Ambito ambito){
        return  ambitoService.save(ambito);
    }

    @PutMapping(consumes = {"application/json"})
    public  Ambito update(@RequestBody Ambito ambito){
        if (ambito.getId()==null);
        return ambitoService.update(ambito);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        ambitoService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Ambito ambito){
        ambitoService.delete(ambito);
    }
}
