package com.example.inforest.service;

import com.example.inforest.model.Persona;
import com.example.inforest.model.Personal;

import java.util.List;

public interface PersonalService {

    List<Personal> getAll();
    Personal get(int id);
    Personal save(Personal personal);
    Personal update(Personal personal);
    void delete(int id);
    void delete(Personal personal);
    Personal findByPersonaId(int id);
}
