package com.example.inforest.repository;

import com.example.inforest.model.Personal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalRepository extends JpaRepository<Personal, Integer> {
    Personal findByPersonaId(int id);
}
