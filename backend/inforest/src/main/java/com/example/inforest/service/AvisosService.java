package com.example.inforest.service;

import com.example.inforest.model.Avisos;

import java.util.List;

public interface AvisosService {

    List<Avisos> getAll();
    Avisos get(int id);
    Avisos save(Avisos avisos);
    Avisos update(Avisos avisos);
    void delete(int id);
    void delete(Avisos avisos);
}
