package com.example.inforest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "personal")
public class Personal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_personal", length = 10)
    private Integer id;

    @Column(name = "funcion", length = 50)
    private String funcion;

    @OneToOne()
    @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
    private Persona persona;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "id_entidad", nullable = false)
@ManyToOne
@JoinColumn(name = "id_entidad")
    private Entidad entidad;

    @JsonIgnore
//    @OneToMany(targetEntity = Actividad.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "personal")
    @OneToMany(mappedBy = "personal")
    private List<Actividad> actividadList;

    @JsonIgnore
    @OneToMany(mappedBy = "personal")
    private List<Escuela> escuelas;

    public Personal() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFuncion() {
        return funcion;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Entidad getEntidad() {
        return entidad;
    }

    public void setEntidad(Entidad entidad) {
        this.entidad = entidad;
    }

    public List<Actividad> getActividadList() {
        return actividadList;
    }

    public void setActividadList(List<Actividad> actividadList) {
        this.actividadList = actividadList;
    }

    public List<Escuela> getEscuelas() {
        return escuelas;
    }

    public void setEscuelas(List<Escuela> escuelas) {
        this.escuelas = escuelas;
    }
}
