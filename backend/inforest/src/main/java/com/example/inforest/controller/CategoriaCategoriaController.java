package com.example.inforest.controller;

import com.example.inforest.model.Categoria;
import com.example.inforest.model.CategoriaCategoria;
import com.example.inforest.service.CategoriaCategoriaService;
import com.example.inforest.service.CategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categoria_categoria")
public class CategoriaCategoriaController {

    @Autowired
    CategoriaCategoriaService categoriaCategoriaService;

    @GetMapping
    public List<CategoriaCategoria> getAll(){
        return categoriaCategoriaService.getAll();
    }

    @GetMapping(params = {"id"})
    public CategoriaCategoria get(@RequestParam int id){
        return categoriaCategoriaService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public CategoriaCategoria save(@RequestBody CategoriaCategoria categoriaCategoria){
        return  categoriaCategoriaService.save(categoriaCategoria);
    }

    @PutMapping(consumes = {"application/json"})
    public  CategoriaCategoria update(@RequestBody CategoriaCategoria categoriaCategoria){
        if (categoriaCategoria.getId()==null);
        return categoriaCategoriaService.update(categoriaCategoria);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        categoriaCategoriaService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody CategoriaCategoria categoriaCategoria){
        categoriaCategoriaService.delete(categoriaCategoria);
    }

}
