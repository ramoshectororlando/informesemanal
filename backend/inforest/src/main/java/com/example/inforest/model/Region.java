package com.example.inforest.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "region")
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_region", length = 10)
    private Integer id;

    @Column(name = "region", length = 10)
    private String region;

//    @JsonBackReference
    @JsonIgnore
//    @OneToMany(targetEntity = Escuela.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "region")
    @OneToMany(mappedBy = "region")
    private List<Escuela> escuelas;

    public Region() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List<Escuela> getEscuelas() {
        return escuelas;
    }

    public void setEscuelas(List<Escuela> escuelas) {
        this.escuelas = escuelas;
    }
}
