package com.example.inforest.service.implement;

import com.example.inforest.model.Persona;
import com.example.inforest.repository.PersonaRepository;
import com.example.inforest.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImplement implements PersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Override
    public List<Persona> getAll() {
        return personaRepository.findAll();
    }

    @Override
    public Persona get(int id) {
        return personaRepository.findById(id).get();
    }

    @Override
    public Persona save(Persona persona) {
        return personaRepository.save(persona);
    }

    @Override
    public Persona update(Persona persona) {
        return personaRepository.save(persona);
    }

    @Override
    public void delete(int id) {
        personaRepository.deleteById(id);
    }

    @Override
    public void delete(Persona persona) {
        personaRepository.delete(persona);
    }

    @Override
    public Persona findByCuil(String cuil) {
        return personaRepository.findByCuil(cuil);
    }
}
