package com.example.inforest.controller;

import com.example.inforest.model.Region;
import com.example.inforest.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/region")
public class RegionController {

    @Autowired
    RegionService regionService;

    @GetMapping
    public List<Region> getAll(){
        return regionService.getAll();
    }

    @GetMapping(params = {"id"})
    public Region get(@RequestParam int id){
        return  regionService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Region save(@RequestBody Region region){
        return  regionService.save(region);
    }

    @PutMapping(consumes = {"application/json"})
    public  Region update(@RequestBody Region region){
        if (region.getId()==null);
        return regionService.update(region);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        regionService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Region region){
        regionService.delete(region);
    }
}
