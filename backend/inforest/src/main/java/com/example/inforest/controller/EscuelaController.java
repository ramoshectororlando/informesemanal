package com.example.inforest.controller;

import com.example.inforest.model.*;
import com.example.inforest.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/escuela")
public class EscuelaController {

    @Autowired
    EscuelaService escuelaService;
    @Autowired
    AmbitoService ambitoService;
    @Autowired
    DomicilioService domicilioService;
    @Autowired
    RegionService regionService;
    @Autowired
    SectorService sectorService;
    @Autowired
    NivelEducativoService nivelEducativoService;

    @GetMapping
    public List<Escuela> getAll(){
        return escuelaService.getAll();
    }

    @GetMapping(params = {"id"})
    public Escuela get(@RequestParam int id){
        return escuelaService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Escuela save(@RequestBody Escuela escuela){
        return  escuelaService.save(verificarSave(escuela));
    }

    @PutMapping(consumes = {"application/json"})
    public Escuela update(@RequestBody Escuela escuela){
        if (escuela.getId()==null);
        return escuelaService.update(verificarUpdate(escuela));
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        escuelaService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Escuela escuela){
        escuelaService.delete(escuela);
    }

    private Escuela verificarSave(Escuela escuela){
        Domicilio domicilio = null;
//        Ambito ambito = null;
//        Region region = null;
//        Sector sector = null;
//        NivelEducativo nivelEducativo = null;
        if(escuela.getDomicilio().getId() == null){
            domicilio = domicilioService.save(escuela.getDomicilio());
        }
//        if(escuela.getAmbito().getId() == null){
//            ambito = ambitoService.save(escuela.getAmbito());
//        }if(escuela.getRegion().getId() == null){
//            region = regionService.save(escuela.getRegion());
//        }if(escuela.getSector().getId() == null){
//            sector = sectorService.save(escuela.getSector());
//        }if(escuela.getNivelEducativo().getId() == null){
//            nivelEducativo = nivelEducativoService.save(escuela.getNivelEducativo());
//        }
        return escuela;
    }

    private Escuela verificarUpdate(Escuela escuela){
        Domicilio domicilio = null;
        if(escuela.getDomicilio().getId() != null){
            domicilio = domicilioService.update(escuela.getDomicilio());
        }
        return escuela;
    }

}
