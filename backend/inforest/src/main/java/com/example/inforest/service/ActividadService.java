package com.example.inforest.service;

import com.example.inforest.model.Actividad;
import com.example.inforest.model.Persona;

import java.util.List;

public interface ActividadService {

    List<Actividad> getAll();
    Actividad get(int id);
    Actividad save(Actividad actividad);
    Actividad update(Actividad actividad);
    void delete(int id);
    void delete(Actividad actividad);
    List<Actividad> findByPersonalId(int id);
}
