package com.example.inforest.controller;

import com.example.inforest.model.Domicilio;
import com.example.inforest.model.Entidad;
import com.example.inforest.service.EntidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/entidad")
public class EntidadController {

    @Autowired
    EntidadService entidadService;

    @GetMapping
    public List<Entidad> getAll(){
        return entidadService.getAll();
    }

    @GetMapping(params = {"id"})
    public Entidad get(@RequestParam int id){
        return  entidadService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Entidad save(@RequestBody Entidad entidad){
        return  entidadService.save(entidad);
    }

    @PutMapping(consumes = {"application/json"})
    public  Entidad update(@RequestBody Entidad entidad){
        if (entidad.getId()==null);
        return entidadService.update(entidad);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        entidadService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Entidad entidad){
        entidadService.delete(entidad);
    }
}
