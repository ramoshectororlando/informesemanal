package com.example.inforest.service;

import com.example.inforest.model.Ambito;
import com.example.inforest.model.Persona;
import com.example.inforest.model.Personal;

import java.util.List;

public interface PersonaService {

    List<Persona> getAll();
    Persona get(int id);
    Persona save(Persona persona);
    Persona update(Persona persona);
    void delete(int id);
    void delete(Persona persona);
    Persona findByCuil(String cuil);
}
