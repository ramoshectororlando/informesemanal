package com.example.inforest.service.implement;

import com.example.inforest.model.Domicilio;
import com.example.inforest.repository.DomicilioRepository;
import com.example.inforest.service.DomicilioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DomicilioServiceImplement implements DomicilioService {

    @Autowired
    private DomicilioRepository domicilioRepository;

    @Override
    public List<Domicilio> getAll() {
        return domicilioRepository.findAll();
    }

    @Override
    public Domicilio get(int id) {
        return domicilioRepository.findById(id).get();
    }

    @Override
    public Domicilio save(Domicilio domicilio) {
        return domicilioRepository.save(domicilio);
    }

    @Override
    public Domicilio update(Domicilio domicilio) {
        return domicilioRepository.save(domicilio);
    }

    @Override
    public void delete(int id) {
        domicilioRepository.deleteById(id);
    }

    @Override
    public void delete(Domicilio domicilio) {
        domicilioRepository.delete(domicilio);
    }
}
