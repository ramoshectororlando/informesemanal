package com.example.inforest.service;

import com.example.inforest.model.Ambito;
import com.example.inforest.model.Categoria;

import java.util.List;

public interface CategoriaService {

    List<Categoria> getAll();
    Categoria get(int id);
    Categoria save(Categoria categoria);
    Categoria update(Categoria categoria);
    void delete(int id);
    void delete(Categoria categoria);
}
