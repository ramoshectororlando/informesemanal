package com.example.inforest.controller;

import com.example.inforest.model.Categoria;
import com.example.inforest.model.Destinatario;
import com.example.inforest.service.DestinatarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/destinatario")
public class DestinatarioController {

    @Autowired
    DestinatarioService destinatarioService;

    @GetMapping
    public List<Destinatario> getAll(){
        return destinatarioService.getAll();
    }

    @GetMapping(params = {"id"})
    public Destinatario get(@RequestParam int id){
        return destinatarioService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Destinatario save(@RequestBody Destinatario destinatario){
        return  destinatarioService.save(destinatario);
    }

    @PutMapping(consumes = {"application/json"})
    public Destinatario update(@RequestBody Destinatario destinatario){
        if (destinatario.getId()==null);
        return destinatarioService.update(destinatario);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        destinatarioService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Destinatario destinatario){
        destinatarioService.delete(destinatario);
    }
}
