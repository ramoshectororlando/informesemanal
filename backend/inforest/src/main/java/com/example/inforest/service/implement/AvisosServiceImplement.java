package com.example.inforest.service.implement;

import com.example.inforest.model.Avisos;
import com.example.inforest.repository.AvisosRepository;
import com.example.inforest.service.AvisosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AvisosServiceImplement implements AvisosService {

    @Autowired
    AvisosRepository avisosRepository;

    @Override
    public List<Avisos> getAll() {
        return avisosRepository.findAll();
    }

    @Override
    public Avisos get(int id) {
        return avisosRepository.findById(id).get();
    }

    @Override
    public Avisos save(Avisos avisos) {
        return avisosRepository.save(avisos);
    }

    @Override
    public Avisos update(Avisos avisos) {
        return avisosRepository.save(avisos);
    }

    @Override
    public void delete(int id) {
        avisosRepository.deleteById(id);
    }

    @Override
    public void delete(Avisos avisos) {
        avisosRepository.delete(avisos);
    }
}
