package com.example.inforest.repository;

import com.example.inforest.model.NivelEducativo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NivelEducativoRepository extends JpaRepository<NivelEducativo, Integer> {
}
