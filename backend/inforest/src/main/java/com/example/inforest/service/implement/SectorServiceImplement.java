package com.example.inforest.service.implement;

import com.example.inforest.model.Region;
import com.example.inforest.model.Sector;
import com.example.inforest.repository.SectorRepository;
import com.example.inforest.service.SectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectorServiceImplement implements SectorService {

    @Autowired
    private SectorRepository sectorRepository;


    @Override
    public List<Sector> getAll() {
        return sectorRepository.findAll();
    }

    @Override
    public Sector get(int id) {
        return sectorRepository.findById(id).get();
    }

    @Override
    public Sector save(Sector sector) {
        return sectorRepository.save(sector);
    }

    @Override
    public Sector update(Sector sector) {
        return sectorRepository.save(sector);
    }

    @Override
    public void delete(int id) {
        sectorRepository.deleteById(id);
    }

    @Override
    public void delete(Sector sector) {
        sectorRepository.delete(sector);
    }
}
