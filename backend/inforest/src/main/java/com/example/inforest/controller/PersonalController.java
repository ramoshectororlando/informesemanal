package com.example.inforest.controller;

import com.example.inforest.model.Actividad;
import com.example.inforest.model.Domicilio;
import com.example.inforest.model.Persona;
import com.example.inforest.model.Personal;
import com.example.inforest.service.PersonaService;
import com.example.inforest.service.PersonalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personal")
public class PersonalController {

    @Autowired
    PersonalService personalService;
    @Autowired
    PersonaService personaService;


    @GetMapping
    public List<Personal> getAll(){
        return personalService.getAll();
    }

    @GetMapping(params = {"id"})
    public Personal get(@RequestParam int id){
        return  personalService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Personal save(@RequestBody Personal personal){
        return  personalService.save(verificarSave(personal));
    }

    @PutMapping(consumes = {"application/json"})
    public  Personal update(@RequestBody Personal personal){
        if (personal.getId()==null);
        return personalService.update(personal);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        personalService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Personal personal){
        personalService.delete(personal);
    }

    private Personal verificarSave(Personal personal){
        Persona persona = null;
        if(personal.getPersona().getId() == null){
            persona = personaService.save(personal.getPersona());
        }
        return personal;
    }

//    private Persona verificarUpdate(Persona persona){
//        Domicilio domicilio = null;
//        if(persona.getDomicilio().getId() != null){
//            domicilio = domicilioService.update(persona.getDomicilio());
//        }
//        return persona;
//    }

    @GetMapping(value = "/cuil",params = {"cuil"})
    public Personal getByIdPersonal(@RequestParam String cuil){
        Persona persona = personaService.findByCuil(cuil);
        Integer idPersona = persona.getId();
        Personal personal = personalService.findByPersonaId(idPersona);
        return personal;
    }

}
