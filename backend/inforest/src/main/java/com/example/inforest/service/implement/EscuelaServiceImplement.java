package com.example.inforest.service.implement;

import com.example.inforest.model.Escuela;
import com.example.inforest.repository.EscuelaRepository;
import com.example.inforest.service.EscuelaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EscuelaServiceImplement implements EscuelaService {

    @Autowired
    private EscuelaRepository escuelaRepository;

    @Override
    public List<Escuela> getAll() {
        return escuelaRepository.findAll();
    }

    @Override
    public Escuela get(int id) {
        return escuelaRepository.findById(id).get();
    }

    @Override
    public Escuela save(Escuela escuela) {
        return escuelaRepository.save(escuela);
    }

    @Override
    public Escuela update(Escuela escuela) {
        return escuelaRepository.save(escuela);
    }

    @Override
    public void delete(int id) {
        escuelaRepository.deleteById(id);
    }

    @Override
    public void delete(Escuela escuela) {
        escuelaRepository.delete(escuela);
    }
}
