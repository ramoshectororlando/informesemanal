package com.example.inforest.service;

import com.example.inforest.model.Entidad;
import com.example.inforest.model.Escuela;

import java.util.List;

public interface EscuelaService {

    List<Escuela> getAll();
    Escuela get(int id);
    Escuela save(Escuela escuela);
    Escuela update(Escuela escuela);
    void delete(int id);
    void delete(Escuela escuela);

}
