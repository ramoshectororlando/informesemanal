package com.example.inforest.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sector")
public class Sector {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_sector", length = 10)
    private Integer id;

    @Column(name = "sector", length = 30)
    private String sector;

//    @JsonBackReference
    @JsonIgnore
//    @OneToMany(targetEntity = Escuela.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "sector")
@OneToMany(mappedBy = "sector")
private List<Escuela> escuelas;

    public Sector() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public List<Escuela> getEscuelas() {
        return escuelas;
    }

    public void setEscuelas(List<Escuela> escuelas) {
        this.escuelas = escuelas;
    }
}
