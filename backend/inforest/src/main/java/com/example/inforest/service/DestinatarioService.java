package com.example.inforest.service;

import com.example.inforest.model.Destinatario;

import java.util.List;

public interface DestinatarioService {

    List<Destinatario> getAll();
    Destinatario get(int id);
    Destinatario save(Destinatario destinatario);
    Destinatario update(Destinatario destinatario);
    void delete(int id);
    void delete(Destinatario destinatario);
}
