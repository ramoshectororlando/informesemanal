package com.example.inforest.controller;

import com.example.inforest.model.Actividad;
import com.example.inforest.model.Avisos;
import com.example.inforest.service.AvisosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/avisos")
public class AvisosController {

    @Autowired
    AvisosService avisosService;

    @GetMapping
    public List<Avisos> getAll(){
        return avisosService.getAll();
    }

    @GetMapping(params = {"id"})
    public Avisos get(@RequestParam int id){
        Avisos avisos = avisosService.get(id);
        return avisosService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Avisos save(@RequestBody Avisos avisos){
        return avisosService.save(avisos);
    }

    @PutMapping(consumes = {"application/json"})
    public Avisos update(@RequestBody Avisos avisos){
        if (avisos.getId()==null);
        return avisosService.update(avisos);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        avisosService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Avisos avisos){
        avisosService.delete(avisos);
    }
}
