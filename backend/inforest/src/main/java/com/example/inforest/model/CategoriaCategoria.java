package com.example.inforest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "categoria_categoria")
public class CategoriaCategoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_categoria_categoria", length = 10)
    private Integer id;

    @Column(name = "detalle",length = 60)
    private String detalle;

    @JsonIgnore
    @OneToMany(mappedBy = "categoriaCategoria")
    private List<Categoria> categorias;

    public CategoriaCategoria() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

}
