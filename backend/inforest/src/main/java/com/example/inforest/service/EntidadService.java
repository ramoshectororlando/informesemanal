package com.example.inforest.service;

import com.example.inforest.model.Entidad;

import java.util.List;

public interface EntidadService {

    List<Entidad> getAll();
    Entidad get(int id);
    Entidad save(Entidad entidad);
    Entidad update(Entidad entidad);
    void delete(int id);
    void delete(Entidad entidad);
}
