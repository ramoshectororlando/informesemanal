package com.example.inforest.service.implement;

import com.example.inforest.model.Entidad;
import com.example.inforest.repository.EntidadRepository;
import com.example.inforest.service.EntidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntidadServiceImplement implements EntidadService {

    @Autowired
    private EntidadRepository entidadRepository;

    @Override
    public List<Entidad> getAll() {
        return entidadRepository.findAll();
    }

    @Override
    public Entidad get(int id) {
        return entidadRepository.findById(id).get();
    }

    @Override
    public Entidad save(Entidad entidad) {
        return entidadRepository.save(entidad);
    }

    @Override
    public Entidad update(Entidad entidad) {
        return entidadRepository.save(entidad);
    }

    @Override
    public void delete(int id) {
        entidadRepository.deleteById(id);
    }

    @Override
    public void delete(Entidad entidad) {
        entidadRepository.delete(entidad);
    }
}
