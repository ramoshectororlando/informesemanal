package com.example.inforest.service.implement;

import com.example.inforest.model.CategoriaCategoria;
import com.example.inforest.repository.CategoriaCategoriaRepository;
import com.example.inforest.repository.CategoriaRepository;
import com.example.inforest.service.CategoriaCategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaCategoriaServiceImplement implements CategoriaCategoriaService {

    @Autowired
    private CategoriaCategoriaRepository categoriaCategoriaRepository;

    @Override
    public List<CategoriaCategoria> getAll() {
        return categoriaCategoriaRepository.findAll();
    }

    @Override
    public CategoriaCategoria get(int id) {
        return categoriaCategoriaRepository.findById(id).get();
    }

    @Override
    public CategoriaCategoria save(CategoriaCategoria categoriaCategoria) {
        return categoriaCategoriaRepository.save(categoriaCategoria);
    }

    @Override
    public CategoriaCategoria update(CategoriaCategoria categoriaCategoria) {
        return categoriaCategoriaRepository.save(categoriaCategoria);
    }

    @Override
    public void delete(int id) {
        categoriaCategoriaRepository.deleteById(id);
    }

    @Override
    public void delete(CategoriaCategoria categoriaCategoria) {
        categoriaCategoriaRepository.delete(categoriaCategoria);
    }
}
