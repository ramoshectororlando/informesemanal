package com.example.inforest.service.implement;

import com.example.inforest.model.Actividad;
import com.example.inforest.repository.ActividadRepository;
import com.example.inforest.service.ActividadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActividadServiceImplement implements ActividadService {

    @Autowired
    ActividadRepository actividadRepository;

    @Override
    public List<Actividad> getAll() {
        return actividadRepository.findAll();
    }

    @Override
    public Actividad get(int id) {
        return actividadRepository.findById(id).get();
    }

    @Override
    public Actividad save(Actividad actividad) {
        return actividadRepository.save(actividad);
    }

    @Override
    public Actividad update(Actividad actividad) {
        return actividadRepository.save(actividad);
    }

    @Override
    public void delete(int id) {
        actividadRepository.deleteById(id);
    }

    @Override
    public void delete(Actividad actividad) {
        actividadRepository.delete(actividad);
    }

    @Override
    public List<Actividad> findByPersonalId(int id) {
        return actividadRepository.findByPersonalId(id);
    }
}
