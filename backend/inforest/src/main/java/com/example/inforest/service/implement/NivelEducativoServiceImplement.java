package com.example.inforest.service.implement;

import com.example.inforest.model.NivelEducativo;
import com.example.inforest.repository.NivelEducativoRepository;
import com.example.inforest.service.NivelEducativoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NivelEducativoServiceImplement implements NivelEducativoService {

    @Autowired
    private NivelEducativoRepository nivelEducativoRepository;

    @Override
    public List<NivelEducativo> getAll() {
        return nivelEducativoRepository.findAll();
    }

    @Override
    public NivelEducativo get(int id) {
        return nivelEducativoRepository.findById(id).get();
    }

    @Override
    public NivelEducativo save(NivelEducativo nivelEducativo) {
        return nivelEducativoRepository.save(nivelEducativo);
    }

    @Override
    public NivelEducativo update(NivelEducativo nivelEducativo) {
        return nivelEducativoRepository.save(nivelEducativo);
    }

    @Override
    public void delete(int id) {
        nivelEducativoRepository.deleteById(id);
    }

    @Override
    public void delete(NivelEducativo nivelEducativo) {
        nivelEducativoRepository.delete(nivelEducativo);
    }
}
