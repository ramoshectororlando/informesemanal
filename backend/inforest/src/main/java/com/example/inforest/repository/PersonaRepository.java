package com.example.inforest.repository;

import com.example.inforest.model.Persona;
import com.example.inforest.model.Personal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Integer> {
    Persona findByCuil(String cuil);
}
