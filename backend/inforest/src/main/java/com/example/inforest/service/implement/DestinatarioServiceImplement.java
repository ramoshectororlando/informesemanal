package com.example.inforest.service.implement;

import com.example.inforest.model.Destinatario;
import com.example.inforest.repository.DestinatarioRepository;
import com.example.inforest.service.DestinatarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestinatarioServiceImplement implements DestinatarioService {

    @Autowired
    private DestinatarioRepository destinatarioRepository;

    @Override
    public List<Destinatario> getAll() {
        return destinatarioRepository.findAll();
    }

    @Override
    public Destinatario get(int id) {
        return destinatarioRepository.findById(id).get();
    }

    @Override
    public Destinatario save(Destinatario destinatario) {
        return destinatarioRepository.save(destinatario);
    }

    @Override
    public Destinatario update(Destinatario destinatario) {
        return destinatarioRepository.save(destinatario);
    }

    @Override
    public void delete(int id) {
        destinatarioRepository.deleteById(id);
    }

    @Override
    public void delete(Destinatario destinatario) {
        destinatarioRepository.delete(destinatario);
    }
}
