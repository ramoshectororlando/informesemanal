package com.example.inforest.repository;

import com.example.inforest.model.Avisos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AvisosRepository extends JpaRepository<Avisos, Integer> {
}
