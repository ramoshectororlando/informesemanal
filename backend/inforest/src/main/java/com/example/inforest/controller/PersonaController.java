package com.example.inforest.controller;

import com.example.inforest.model.Ambito;
import com.example.inforest.model.Domicilio;
import com.example.inforest.model.Persona;
import com.example.inforest.model.Personal;
import com.example.inforest.service.DomicilioService;
import com.example.inforest.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@RestController
@RequestMapping("/persona")
public class PersonaController {

    @Autowired
    PersonaService personaService;
    @Autowired
    DomicilioService domicilioService;

    @GetMapping
    public List<Persona> getAll(){
        return personaService.getAll();
    }

    @GetMapping(params = {"id"})
    public Persona get(@RequestParam int id){
        return  personaService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Persona save(@RequestBody Persona persona){
        return  personaService.save(verificarSave(persona));
    }

    @PutMapping(consumes = {"application/json"})
    public  Persona update(@RequestBody Persona persona){
        if (persona.getId()==null);
        return personaService.update(verificarUpdate(persona));
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        personaService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Persona persona){
        personaService.delete(persona);
    }

    private Persona verificarSave(Persona persona){
        Domicilio domicilio = null;
        if(persona.getDomicilio().getId() == null){
            domicilio = domicilioService.save(persona.getDomicilio());
        }
        return persona;
    }

    private Persona verificarUpdate(Persona persona){
        Domicilio domicilio = null;
        if(persona.getDomicilio().getId() != null){
            domicilio = domicilioService.update(persona.getDomicilio());
        }
        return persona;
    }

//    @GetMapping(value = "/cuil",params = {"cuil"})
//    public Boolean get(@RequestParam String cuil){
//        Boolean flag=false;
//        try {
//            Persona persona = personaService.findByCuil(cuil);
//            if(persona==null){
//                flag=false;
//            }else {
//                flag=true;
//            }
//        }catch (Exception e){
//
//        }
//        return flag;
//    }

    @GetMapping(value = "/cuil",params = {"cuil"})
    public Persona getPersonaCuil(@RequestParam String cuil){
        Persona persona = personaService.findByCuil(cuil);
        return persona;
    }
}
