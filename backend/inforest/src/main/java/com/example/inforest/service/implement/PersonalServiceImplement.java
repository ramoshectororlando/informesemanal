package com.example.inforest.service.implement;

import com.example.inforest.model.Personal;
import com.example.inforest.repository.PersonalRepository;
import com.example.inforest.service.PersonalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonalServiceImplement implements PersonalService {

    @Autowired
    private PersonalRepository personalRepository;

    @Override
    public List<Personal> getAll() {
        return personalRepository.findAll();
    }

    @Override
    public Personal get(int id) {
        return personalRepository.findById(id).get();
    }

    @Override
    public Personal save(Personal personal) {
        return personalRepository.save(personal);
    }

    @Override
    public Personal update(Personal personal) {
        return personalRepository.save(personal);
    }

    @Override
    public void delete(int id) {
        personalRepository.deleteById(id);
    }

    @Override
    public void delete(Personal personal) {
        personalRepository.delete(personal);
    }

    @Override
    public Personal findByPersonaId(int id) {
        return personalRepository.findByPersonaId(id);
    }

}
