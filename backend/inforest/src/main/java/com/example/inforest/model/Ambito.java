package com.example.inforest.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ambito")
public class Ambito {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ambito", length = 10)
    private Integer id;

    @Column(name = "ambito", length = 25)
    private String ambito;

//    @JsonBackReference
    @JsonIgnore
//    @OneToMany(targetEntity = Escuela.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "ambito")
@OneToMany(mappedBy = "ambito")
private List<Escuela> escuelas;

    public Ambito() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public List<Escuela> getEscuelas() {
        return escuelas;
    }

    public void setEscuelas(List<Escuela> escuelas) {
        this.escuelas = escuelas;
    }
}
