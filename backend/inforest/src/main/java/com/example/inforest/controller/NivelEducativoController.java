package com.example.inforest.controller;

import com.example.inforest.model.Domicilio;
import com.example.inforest.model.NivelEducativo;
import com.example.inforest.service.NivelEducativoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/nivel_educativo")
public class NivelEducativoController {

    @Autowired
    NivelEducativoService nivelEducativoService;

    @GetMapping
    public List<NivelEducativo> getAll(){
        return nivelEducativoService.getAll();
    }

    @GetMapping(params = {"id"})
    public NivelEducativo get(@RequestParam int id){
        return  nivelEducativoService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public NivelEducativo save(@RequestBody NivelEducativo nivelEducativo){
        return  nivelEducativoService.save(nivelEducativo);
    }

    @PutMapping(consumes = {"application/json"})
    public  NivelEducativo update(@RequestBody NivelEducativo nivelEducativo){
        if (nivelEducativo.getId()==null);
        return nivelEducativoService.update(nivelEducativo);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        nivelEducativoService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody NivelEducativo nivelEducativo){
        nivelEducativoService.delete(nivelEducativo);
    }
}
