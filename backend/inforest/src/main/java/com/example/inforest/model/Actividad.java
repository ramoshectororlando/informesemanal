package com.example.inforest.model;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name = "actividad")
public class Actividad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_actividad", length = 10)
    private Integer id;

    @Column(name = "fecha", length = 25)
    private String fecha;

    @Column(name = "hora")
    private String hora;

    @Column(name = "detalle", length = 2000)
    private String detalle;

    @Column(name = "cant_participantes", length = 5)
    private int cantidadParticipantes;

    @Column(name = "duracion_hora", length = 10)
    private int duracionHora;

    @Column(name = "duracion_minuto", length = 10)
    private int duracionMinuto;

    @Column(name = "activo", length = 5)
    private Boolean activo;

    @ManyToOne
    @JoinColumn(name = "id_personal")
    private Personal personal;

    @ManyToOne
    @JoinColumn(name = "id_escuela")
    private Escuela escuela;

    @ManyToOne
    @JoinColumn(name = "id_categoria")
    private Categoria categoria;

    @ManyToOne
    @JoinColumn(name = "id_destinatario")
    private Destinatario destinatario;

    @Column(name = "fecha_carga", length = 25)
    private String fechaCarga;

    public Actividad() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getCantidadParticipantes() {
        return cantidadParticipantes;
    }

    public void setCantidadParticipantes(int cantidadParticipantes) {
        this.cantidadParticipantes = cantidadParticipantes;
    }

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public Escuela getEscuela() {
        return escuela;
    }

    public void setEscuela(Escuela escuela) {
        this.escuela = escuela;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Destinatario getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(Destinatario destinatario) {
        this.destinatario = destinatario;
    }

    public int getDuracionHora() {
        return duracionHora;
    }

    public void setDuracionHora(int duracionHora) {
        this.duracionHora = duracionHora;
    }

    public int getDuracionMinuto() {
        return duracionMinuto;
    }

    public void setDuracionMinuto(int duracionMinuto) {
        this.duracionMinuto = duracionMinuto;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(String fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

}
