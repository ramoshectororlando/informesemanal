package com.example.inforest.repository;

import com.example.inforest.model.CategoriaCategoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaCategoriaRepository extends JpaRepository<CategoriaCategoria, Integer> {
}
