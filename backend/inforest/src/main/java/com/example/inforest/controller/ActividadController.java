package com.example.inforest.controller;

import com.example.inforest.model.Actividad;
import com.example.inforest.model.Persona;
import com.example.inforest.model.Personal;
import com.example.inforest.service.ActividadService;
import com.example.inforest.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/actividad")
public class ActividadController {

    @Autowired
    ActividadService actividadService;
    @Autowired
    PersonaService personaService;

    @GetMapping
    public List<Actividad> getAll(){
        return actividadService.getAll();
    }

    @GetMapping(params = {"id"})
    public Actividad get(@RequestParam int id){
        Actividad actividad = actividadService.get(id);
        return actividadService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Actividad save(@RequestBody Actividad actividad){
        return actividadService.save(actividad);
    }

    @PutMapping(consumes = {"application/json"})
    public Actividad update(@RequestBody Actividad actividad){
        if (actividad.getId()==null);
        return actividadService.update(actividad);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        actividadService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Actividad actividad){
        actividadService.delete(actividad);
    }

    @GetMapping(value = "/cuil",params = {"cuil"})
    public List<Actividad> getAllByIdPersonal(@RequestParam String cuil){
        Persona persona = personaService.findByCuil(cuil);
        Integer idPersona = persona.getId();
        return actividadService.findByPersonalId(idPersona);
    }

}
