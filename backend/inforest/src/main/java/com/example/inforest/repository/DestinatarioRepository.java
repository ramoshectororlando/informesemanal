package com.example.inforest.repository;

import com.example.inforest.model.Destinatario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DestinatarioRepository extends JpaRepository<Destinatario, Integer> {
}
