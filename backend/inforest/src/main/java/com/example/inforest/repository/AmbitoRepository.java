package com.example.inforest.repository;

import com.example.inforest.model.Ambito;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmbitoRepository extends JpaRepository<Ambito, Integer> {
}
