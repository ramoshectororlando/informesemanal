package com.example.inforest.service;

import com.example.inforest.model.Categoria;
import com.example.inforest.model.CategoriaCategoria;

import java.util.List;

public interface CategoriaCategoriaService {

    List<CategoriaCategoria> getAll();
    CategoriaCategoria get(int id);
    CategoriaCategoria save(CategoriaCategoria categoriaCategoria);
    CategoriaCategoria update(CategoriaCategoria categoriaCategoria);
    void delete(int id);
    void delete(CategoriaCategoria categoriaCategoria);

}
