package com.example.inforest.repository;

import com.example.inforest.model.Escuela;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EscuelaRepository extends JpaRepository<Escuela, Integer> {
}
