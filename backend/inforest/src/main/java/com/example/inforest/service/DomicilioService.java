package com.example.inforest.service;

import com.example.inforest.model.Domicilio;

import java.util.List;

public interface DomicilioService {

    List<Domicilio> getAll();
    Domicilio get(int id);
    Domicilio save(Domicilio domicilio);
    Domicilio update(Domicilio domicilio);
    void delete(int id);
    void delete(Domicilio domicilio);
}
