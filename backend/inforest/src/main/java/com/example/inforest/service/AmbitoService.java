package com.example.inforest.service;

import com.example.inforest.model.Ambito;

import java.util.List;

public interface AmbitoService {

    List<Ambito> getAll();
    Ambito get(int id);
    Ambito save(Ambito ambito);
    Ambito update(Ambito ambito);
    void delete(int id);
    void delete(Ambito ambito);
}
