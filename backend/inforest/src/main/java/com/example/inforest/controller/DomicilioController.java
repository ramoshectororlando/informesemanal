package com.example.inforest.controller;

import com.example.inforest.model.Domicilio;
import com.example.inforest.service.DomicilioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/domicilio")
public class DomicilioController {

    @Autowired
    DomicilioService domicilioService;

    @GetMapping
    public List<Domicilio>getAll(){
        return domicilioService.getAll();
    }

    @GetMapping(params = {"id"})
    public Domicilio get(@RequestParam int id){
        return  domicilioService.get(id);
    }

    @PostMapping(consumes = {"application/json"})
    public Domicilio save(@RequestBody Domicilio domicilio){
        return  domicilioService.save(domicilio);
    }

    @PutMapping(consumes = {"application/json"})
    public  Domicilio update(@RequestBody Domicilio domicilio){
        if (domicilio.getId()==null);
        return domicilioService.update(domicilio);
    }

    @DeleteMapping(value = {"/{id}"})
    public void delete(@PathVariable("id") int id){
        domicilioService.delete(id);
    }

    @DeleteMapping
    public void delete(@RequestBody Domicilio domicilio){
        domicilioService.delete(domicilio);
    }
}
