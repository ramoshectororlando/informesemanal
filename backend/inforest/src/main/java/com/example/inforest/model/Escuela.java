package com.example.inforest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "escuela")
public class Escuela {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_escuela", length = 10)
    private Integer id;

    @Column(name = "cue", length = 15)
    private String cue;

    @Column(name = "nombre", length = 50)
    private String nombre;

    @Column(name = "telefono", length = 30)
    private String telefono;

    @Column(name = "email", length = 40)
    private String email;

    @OneToOne()
    @JoinColumn(name = "id_domicilio", referencedColumnName = "id_domicilio")
    private Domicilio domicilio;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "id_region", nullable = false)
    @ManyToOne
    @JoinColumn(name = "id_region")
    private Region region;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "id_nivel", nullable = false)
    @ManyToOne
    @JoinColumn(name = "id_nivel")
    private NivelEducativo nivelEducativo;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "id_sector", nullable = false)
    @ManyToOne
    @JoinColumn(name = "id_sector")
    private Sector sector;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "id_ambito", nullable = false)
    @ManyToOne
    @JoinColumn(name = "id_ambito")
    private Ambito ambito;

//    @JsonIgnore
//    @OneToMany(mappedBy = "escuela")
//    private List<Persona> personaList;

//    @OneToOne()
//    @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
//    private Persona persona;

    @JsonIgnore
//    @OneToMany(targetEntity = Actividad.class, cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "escuela")
    @OneToMany(mappedBy = "escuela")
    private List<Actividad> actividadList;

    @ManyToOne
    @JoinColumn(name = "id_persona")
    private Persona persona;

    @ManyToOne
    @JoinColumn(name = "id_personal")
    private Personal personal;

    public Escuela() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCue() {
        return cue;
    }

    public void setCue(String cue) {
        this.cue = cue;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public NivelEducativo getNivelEducativo() {
        return nivelEducativo;
    }

    public void setNivelEducativo(NivelEducativo nivelEducativo) {
        this.nivelEducativo = nivelEducativo;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public Ambito getAmbito() {
        return ambito;
    }

    public void setAmbito(Ambito ambito) {
        this.ambito = ambito;
    }

    public List<Actividad> getActividadList() {
        return actividadList;
    }

    public void setActividadList(List<Actividad> actividadList) {
        this.actividadList = actividadList;
    }

//    public List<Persona> getPersonaList() {
//        return personaList;
//    }
//
//    public void setPersonaList(List<Persona> personaList) {
//        this.personaList = personaList;
//    }


    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }
}
