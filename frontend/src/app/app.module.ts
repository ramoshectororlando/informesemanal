import { InterceptorService } from './services/interceptor.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListadoEscuelasComponent } from './components/listado-escuelas/listado-escuelas.component';
import { ListadoPersonasComponent } from './components/listado-personas/listado-personas.component';
import { AgregarEscuelaComponent } from './components/agregar-escuela/agregar-escuela.component';
import { AgregarPersonaComponent } from './components/agregar-persona/agregar-persona.component';
import { InformeComponent } from './components/informe/informe.component';
import { ListadoInformesComponent } from './components/listado-informes/listado-informes.component';
import { FormsModule } from '@angular/forms';
import { ValidacionComponent } from './components/validacion/validacion.component';
import { MenuComponent } from './components/menu/menu.component';

// Import pdfmake-wrapper and the fonts to use
import { PdfMakeWrapper } from 'pdfmake-wrapper';
import pdfFonts from "pdfmake/build/vfs_fonts";
import { MenuAdminComponent } from './components/menu-admin/menu-admin.component'; // fonts provided for pdfmake

import {NgxPaginationModule} from 'ngx-pagination';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { SubCategoriasComponent } from './components/sub-categorias/sub-categorias.component';
import { AgregarPersonalComponent } from './components/agregar-personal/agregar-personal.component';
import { FilterSearchPipe } from './pipes/filter-search.pipe';
import { FilterSearchPersonPipe } from './pipes/filter-search-person.pipe';
import { FilterSearchInformPipe } from './pipes/filter-search-inform.pipe';
import { FilterSearchNomPipe } from './pipes/filter-search-nom.pipe';
import { FilterSearchEscNomPipe } from './pipes/filter-search-esc-nom.pipe';
import { FilterSearchInfEscPipe } from './pipes/filter-search-inf-esc.pipe';
import { FilterSearchDeptoPipe } from './pipes/filter-search-depto.pipe';
import { MenuFacilitadorComponent } from './components/menu-facilitador/menu-facilitador.component';
import { ListadoInformesFacilitadorComponent } from './components/listado-informes-facilitador/listado-informes-facilitador.component';

import { NgxSpinnerModule } from 'ngx-spinner';
import { ListadoInformesAdminComponent } from './components/listado-informes-admin/listado-informes-admin.component';
import { InformeAdminComponent } from './components/informe-admin/informe-admin.component';
import { AvisosComponent } from './components/avisos/avisos.component';
import { AvisosAdminComponent } from './components/avisos-admin/avisos-admin.component';
import { AvisosFacilitadorComponent } from './components/avisos-facilitador/avisos-facilitador.component';
import { EstadisticasComponent } from './components/estadisticas/estadisticas.component';
import { EstadisticasAdminComponent } from './components/estadisticas-admin/estadisticas-admin.component';
import { EstadisticasFacilitadorComponent } from './components/estadisticas-facilitador/estadisticas-facilitador.component';
import { ConsultasRteComponent } from './components/consultas-rte/consultas-rte.component';
import { ConsultasRteFacilitadorComponent } from './components/consultas-rte-facilitador/consultas-rte-facilitador.component';

// Set the fonts to use
PdfMakeWrapper.setFonts(pdfFonts);

@NgModule({
  declarations: [
    AppComponent,
    ListadoEscuelasComponent,
    ListadoPersonasComponent,
    AgregarEscuelaComponent,
    AgregarPersonaComponent,
    InformeComponent,
    ListadoInformesComponent,
    ValidacionComponent,
    MenuComponent,
    MenuAdminComponent,
    CategoriasComponent,
    SubCategoriasComponent,
    AgregarPersonalComponent,
    FilterSearchPipe,
    FilterSearchPersonPipe,
    FilterSearchInformPipe,
    FilterSearchNomPipe,
    FilterSearchEscNomPipe,
    FilterSearchInfEscPipe,
    FilterSearchDeptoPipe,
    MenuFacilitadorComponent,
    ListadoInformesFacilitadorComponent,
    ListadoInformesAdminComponent,
    InformeAdminComponent,
    AvisosComponent,
    AvisosAdminComponent,
    AvisosFacilitadorComponent,
    EstadisticasComponent,
    EstadisticasAdminComponent,
    EstadisticasFacilitadorComponent,
    ConsultasRteComponent,
    ConsultasRteFacilitadorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    AutocompleteLibModule,
    NgxSpinnerModule,
    ToastrModule.forRoot()
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
