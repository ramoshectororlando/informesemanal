import { Avisos } from './../../model/avisos.model';
import { AvisosService } from './../../services/avisos.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

import _ from "lodash";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-avisos-admin',
  templateUrl: './avisos-admin.component.html',
  styleUrls: ['./avisos-admin.component.css']
})
export class AvisosAdminComponent implements OnInit {

  avisosList: Avisos[] = [];
  aviso: Avisos = new Avisos();

  fechaHoy: string = new Date().toLocaleDateString();
  pageActual: number = 1;

  constructor(private modalService: NgbModal,
              private avisosService: AvisosService,
              private toastr: ToastrService
    ) { }

  ngOnInit(): void {
    this.traerAvisos();
  }

  verModal(abrirModal) {
    this.modalService.open(abrirModal)
  }

  traerAvisos(){
    this.avisosService.getAll().subscribe(
      response=>{
        // this.avisosList = response;
        response.forEach(x=>{
          if(x.eliminado == false){
            this.avisosList.push(x)
          }
        })
      }
    )
  }

  datosModal(modal) {
    let tituloModalDato = (<HTMLInputElement>document.getElementById('asunto')).value;
    let mensajeModalDato = (<HTMLInputElement>document.getElementById('mensaje')).value;
    this.aviso.titulo = tituloModalDato;
    this.aviso.mensaje = mensajeModalDato;
    this.aviso.fecha = this.fechaHoy;
    this.aviso.eliminado = false;
    this.aviso.leido = false;
    if(tituloModalDato != "" && mensajeModalDato != ""){
      this.avisosService.add(this.aviso).subscribe(
        respo=>{
          console.log("aviso creado")
        }
      )
    modal.close();
    window.location.reload();
    }else{
      this.toastr.info('Debe ingresar los campos requeridos', 'Atención')
    }
  }

  listaAvisosOrdenados() {
    return _.sortBy(this.avisosList, 'fecha').reverse();
  }

  modificarAviso(id,abrirModalModif){
    this.modalService.open(abrirModalModif)

    this.avisosService.get(id).subscribe(
      response => {
        this.aviso = response;
        (<HTMLInputElement>document.getElementById('asunto')).value = this.aviso.titulo;
        (<HTMLInputElement>document.getElementById('mensaje')).value = this.aviso.mensaje;
      }
    )
  }

  datosModalUpdate(modal2){
    let tituloModalDatoUpdate = (<HTMLInputElement>document.getElementById('asunto')).value;
    let mensajeModalDatoUpdate = (<HTMLInputElement>document.getElementById('mensaje')).value;
    this.aviso.titulo = tituloModalDatoUpdate;
    this.aviso.mensaje = mensajeModalDatoUpdate;
    this.avisosService.update(this.aviso).subscribe(
      respo => {
        console.log(" Aviso actualizado...")
      }
    )
    modal2.close();
    window.location.reload();
  }

  eliminarAviso(id){
    if (confirm("Está seguro que desea eliminar el aviso?")) {
    this.avisosService.get(id).subscribe(
      res=>{
        this.aviso = res
        this.aviso.eliminado = true;
        this.aviso.leido = true;
        this.avisosService.update(this.aviso).subscribe(
          respo=>{
            console.log("aviso eliminado")
          }
        )
      }
    )
    window.location.reload();
  }
}

}
