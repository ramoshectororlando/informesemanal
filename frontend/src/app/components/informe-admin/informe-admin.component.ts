import { DestinatarioService } from './../../services/destinatario.service';
import { Destinatario } from './../../model/destinatario.model';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Categoria } from './../../model/categoria.model';
import { CategoriaService } from './../../services/categoria.service';
import { Actividad } from './../../model/actividad.model';
import { Escuela } from './../../model/escuela.model';
import { Personal } from './../../model/personal.model';
import { ActividadService } from './../../services/actividad.service';
import { PersonalService } from './../../services/personal.service';
import { EscuelaService } from './../../services/escuela.service';
import { Component, OnInit } from '@angular/core';
import { ValidacionService } from 'src/app/services/validacion.service';

import _ from "lodash";

@Component({
  selector: 'app-informe-admin',
  templateUrl: './informe-admin.component.html',
  styleUrls: ['./informe-admin.component.css']
})
export class InformeAdminComponent implements OnInit {

  personalList: Personal[] = [];
  escuelaList: Escuela[];
  categoriaList: Categoria[];
  destinatarioList: Destinatario[];

  actividad: Actividad = new Actividad();
  categoria: Categoria = new Categoria();
  escuela: Escuela = new Escuela();
  personal: Personal = new Personal();
  destinatario: Destinatario = new Destinatario();
  fechaHoy: string = new Date().toLocaleTimeString();
  fechaCargaHoy: string = new Date().toLocaleDateString();
  diaC: string;
  mesC: string;
  añoC: string;
  formatoFecha: string;

  menu: boolean;
  dato: string;
  public codeValue: string;
  cadenaCue: string;
  idEscuela: number;

  public keyword = "nombre";
  public data$: Observable<any[]>;

  constructor(private escuelaService: EscuelaService,
    private personalService: PersonalService,
    private actividadService: ActividadService,
    private categoriaService: CategoriaService,
    private destinatarioService: DestinatarioService,
    private router: Router,
    private validacionService: ValidacionService,
    private activatedRoute: ActivatedRoute) {
    this.findById(activatedRoute.snapshot.params['id'])
  }

  ngOnInit(): void {
    this.traerPersonal();
    this.traerEscuela();
    this.traerCategoria();
    //this.traerSubCategoria();
    // this.menuValidacion();
    this.traerDestinatario();
  }

  // traerPersonal() {
  //   this.personalService.getAll().subscribe(
  //     response => {
  //       response.forEach(x => {
  //         if ((x.persona.cuil == this.dato) && (x.funcion == 'RTE ESCUELA')) {            
  //           this.personalList.push(x);
  //           this.personal.id = x.persona.id;            
  //         } else
  //           if ((this.dato == 'ramos123h') && (x.funcion == 'RTE ESCUELA')) {
  //             this.personalList.push(x);
  //           }
  //       })
  //     }
  //   )
  // }

  traerPersonal() {
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      response => {
          if (response.funcion == "ADMINISTRADOR") {
            this.personalService.getAll().subscribe(
              res => {
                this.personalList = res;
              }
            )
          }
      }
    )
  }

  listaPersonalOrdenada() {
    return _.sortBy(this.personalList, 'persona.apellido');
  }

  traerEscuela() {
    this.escuelaService.getAll().subscribe(
      response => {
        this.escuelaList = response;
      }
    )
  }

  traerCategoria() {
    this.categoriaService.getAll().subscribe(
      response => {
        this.categoriaList = response;
      }
    )
  }

  listaCategoriaOrdenada() {
    return _.sortBy(this.categoriaList, 'categoria');
  }

  traerDestinatario() {
    this.destinatarioService.getAll().subscribe(
      response => {
        this.destinatarioList = response;
      }
    )
  }

  listaDestinatarioOrdenada() {
    return _.sortBy(this.destinatarioList, 'destinatario');
  }

  agregarActividadAdmin() {
    this.actividad.categoria = this.categoria;
    this.actividad.destinatario = this.destinatario;
    this.actividad.escuela = this.escuela;
    this.actividad.personal = this.personal;
    this.actividad.activo = true;
    this.actividad.hora = this.fechaHoy;
    
    this.diaC = this.fechaCargaHoy.substring(0,2)
    this.mesC = this.fechaCargaHoy.substring(3,5)
    this.añoC = this.fechaCargaHoy.substring(6,10)
    this.formatoFecha = this.añoC+"-"+this.mesC+"-"+this.diaC
    
    this.actividad.fechaCarga = this.formatoFecha;
    this.actividadService.add(this.actividad).subscribe(
      response => {
        console.log("actividad agregada...");
        this.router.navigate(["/listado-informes-AdminAdmin"])
      },
    )
  }

  findById(id) {
    this.actividadService.get(id).subscribe(
      response => {
        this.actividad = response;
        this.categoria = this.actividad.categoria;
        this.destinatario = this.actividad.destinatario;
        this.escuela = this.actividad.escuela;
        this.personal = this.actividad.personal;
      },
      err => console.log(err)
    );
  }

  actualizarActividadAdmin() {
    this.actividadService.update(this.actividad).subscribe(
      response => {
        console.log("actividad actualizada...")
        this.router.navigate(['/listado-informes-AdminAdmin'])
      }
    )
  }

  menuValidacion() {
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      response => {
        if (response == null) {
          this.router.navigate(['/'])
        } else
          if (response.funcion == "ADMINISTRADOR") {
            this.menu = true;
          } else {
            this.menu = false;
          }
      }
    )
  }

  public saveCode(e): void {
    let cue = e.target.value;
    cue = cue.substring(0, 9)
    let listt = this.escuelaList.filter(x =>
      x.cue === cue)[0];
    this.idEscuela = listt.id;
    this.escuela.id = this.idEscuela;
  }

  // selectEvent(item) {
  //   this.idEscuela = item.id;
  //   this.escuela.id = this.idEscuela;
  //   console.log("item...." + this.escuela.id);
  // }

  recargar() {
    window.location.reload();
  }

}
