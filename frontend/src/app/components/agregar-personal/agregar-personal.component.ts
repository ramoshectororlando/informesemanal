import { PersonalService } from './../../services/personal.service';
import { Personal } from './../../model/personal.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from 'src/app/model/persona.model';
import { PersonaService } from 'src/app/services/persona.service';

@Component({
  selector: 'app-agregar-personal',
  templateUrl: './agregar-personal.component.html',
  styleUrls: ['./agregar-personal.component.css']
})
export class AgregarPersonalComponent implements OnInit {

  menu: boolean;
  dato: string;

  personaList: Persona[];
  personalList: Personal[];
  personaPersonalList: Persona[] = [];
  ids:number [] = [];
  persona: Persona = new Persona();
  personal: Personal = new Personal();

  constructor(private router: Router,
    private personaService: PersonaService,
    private personalService: PersonalService) { }

  ngOnInit(): void {
    this.menuValidacion();
    this.obtenerPersonas();
    this.obtenerPersonal();
  }

  obtenerPersonas() {
    this.personaService.getAll().subscribe(
      response => {
        this.personaList = response;
      }
    )
  }

  // obtenerPersonal() {
  //   this.personalService.getAll().subscribe(
  //     response => {
  //       this.personalList = response;
  //       this.personaPersonalList = this.personaList.filter(x=>
  //         this.personalList.forEach(y=>{
  //           x.id === y.persona.id
  //         })
  //         )
  //       });
  //     }
  obtenerPersonal() {
    this.personalService.getAll().subscribe(
      response => {
        this.personalList = response;
        this.personalList.forEach(x=>{
          this.ids.push(x.persona.id);
        })
        this.personaList.forEach(y=>{
          if(!(this.ids.includes(y.id))){
            this.personaPersonalList.push(y)
          }
        });
        })
      }

      agregarPersonal() {
        this.personal.persona = this.persona;
        this.personal.entidad = null;
        this.personalService.add(this.personal).subscribe(
          response => {
            console.log("personal asignado...");
            this.router.navigate(["/listado-personas"])
          },
        )
      }

      menuValidacion() {
        this.dato = sessionStorage.getItem('usuarioo')
        this.personalService.getPersonalCuil(this.dato).subscribe(
          response=>{
            if (response == null) {
              this.router.navigate(['/'])
            } else
              if (response.funcion == "ADMINISTRADOR") {
                this.menu = true;
              } else {
                this.menu = false;
              }
          }
        )
      }

}
