import { ActividadService } from './../../services/actividad.service';
import { EscuelaService } from './../../services/escuela.service';
import { Escuela } from 'src/app/model/escuela.model';
import { Component, OnInit } from '@angular/core';
import { Personal } from 'src/app/model/personal.model';
import { PersonalService } from 'src/app/services/personal.service';

import pdfMake from 'pdfmake/build/pdfmake';
import _ from "lodash";
import { Actividad } from 'src/app/model/actividad.model';

@Component({
  selector: 'app-consultas-rte',
  templateUrl: './consultas-rte.component.html',
  styleUrls: ['./consultas-rte.component.css']
})
export class ConsultasRteComponent implements OnInit {

  dato: string;
  personalList: Personal[] = [];
  personal: Personal = new Personal();
  valorSeleccionado: number;
  escuelas: Escuela[] = [];

  pageActual: number = 1;

  actividadList: Actividad[] = [];
  actividadListFilter: Actividad[] = [];

  fechasActividades: String[] = [];
  fechaHoy: Date = new Date();
  mesHoy: String;
  visitasList: String[] = [];
  temp: String = "NO";

  cantidadActividades: number = 0;
  cantidadHoras: number = 0;
  cantidadMinutos: number = 0;
  decimal: number;
  entero: number;
  decimal2: number;
  entero2: number;
  promedioHoras: number = 0;
  temporalMinutos: number = 0;
  temporalPromedio: number = 0;
  contadorActividades: Actividad[];
  fechasActividadesFilter: string[];
  fechasUnicas: string[];
  cantidadFechas: number = 0;
  cantidadHorasPromedio = 0;
  entero2Promedio = 0;
  decimal5: number;
  entero5: number;
  decimal25: number;
  entero25: number;

  escuela: Escuela;
  visit: string;

  isDisabled: boolean = true;

  año: number = 2021;
  mes: number = 10;

  // diasMes: number = new Date(this.año, this.mes, 0).getDate();
  // diasSemana: string[] = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];

  diasMes: number;
  diasSemana: string[];

  diass: string[]=[];
  fechass: number[]=[];
  promedios: number[]=[];
  diasSS: string;
  fechaSS: string;
  promedioSS: string;

  constructor(private personalService: PersonalService,
    private escuelaService: EscuelaService,
    private actividadService: ActividadService
  ) { }

  ngOnInit(): void {
    this.traerPersonal();
  }

  traerPersonal() {
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      response => {
        if (response.funcion == "ADMINISTRADOR") {
          this.personalService.getAll().subscribe(
            res => {
              res.forEach(x => {
                if (x.funcion == "RTE ESCUELA") {
                  this.personalList.push(x);
                }
              })
              // this.personalList = res;
            }
          )
        }
      }
    )
  }

  listaPersonalOrdenada() {
    return _.sortBy(this.personalList, 'persona.apellido');
  }

  capturar() {
    this.valorSeleccionado = this.personal.id;
  }

  traerEscuelas(id) {
    this.isDisabled = false
    this.escuelas = []
    this.visitasList = []
    this.cantidadHoras = 0;
    this.cantidadMinutos = 0;
    this.cantidadActividades = 0;
    this.promedioHoras = 0;
    this.temporalMinutos = 0;
    this.temporalPromedio = 0
    this.diass = [];
    this.fechass = [];
    this.promedios = [];
    this.contadorActividades = [];
    this.fechasActividadesFilter = [];
    this.fechasUnicas = [];
    this.cantidadFechas = 0;
    this.cantidadHorasPromedio = 0;
    this.entero2Promedio = 0;
    this.escuelaService.getAll().subscribe(
      response => {
        response.forEach(x => {
          if (x.personal?.id == id) {
            this.escuelas.push(x)
          }
        })

        this.personalService.get(id).subscribe(
          response => {
            this.actividadService.getAllByPersonalId(response.persona.cuil).subscribe(
              resp => {
                this.actividadList = resp;

                this.mesHoy = this.fechaHoy.toLocaleDateString().toString().substring(3, 5)
                this.escuelas.forEach(escu => {
                  this.temp = "NO"
                  this.actividadList.forEach(res => {
                    if ((res.fecha.toString().substring(5, 7)) == this.mesHoy) {
                      if (escu.cue == res.escuela.cue) {
                        this.temp = "SI"
                      }
                    }
                  })
                  
                  this.visitasList.push(this.temp)
                })
              }
            )


            // this.actividadService.getAllByPersonalId(response.persona.cuil).subscribe(
            //   respon=>{
            //     this.actividadList = respon
            //     this.mesHoy = this.fechaHoy.toLocaleDateString().toString().substring(3, 5)
                
            //     this.mes = Number(this.mesHoy)
            //     this.diasMes= new Date(this.año, this.mes, 0).getDate();
            //     this.diasSemana= ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
            //       for (var dia = 1; dia <= this.diasMes; dia++) {
            //         this.cantidadHoras = 0;
            //         this.cantidadMinutos = 0;
            //         this.cantidadActividades = 0;
            //         this.promedioHoras = 0;
            //         this.temporalMinutos = 0;
            //         this.temporalPromedio = 0;

            //       this.actividadList.forEach(re=>{
            //         if ((re.fecha.toString().substring(5, 7)) == this.mesHoy){
            //           if(dia == (Number(re.fecha.toString().substring(8, 10)))){
            //             this.cantidadActividades = this.cantidadActividades + 1
            //             this.cantidadHoras = this.cantidadHoras + re.duracionHora
            //             this.cantidadMinutos = this.cantidadMinutos + re.duracionMinuto
            //           }else{
            //             this.cantidadActividades = this.cantidadActividades + 0
            //             this.cantidadHoras = this.cantidadHoras + 0
            //             this.cantidadMinutos = this.cantidadMinutos + 0
            //           }
            //         }
            //       })

            //       this.cantidadMinutos = this.cantidadMinutos / 60;
            //       this.decimal = (this.cantidadMinutos % 1);
            //       this.entero = this.cantidadMinutos - this.decimal;
            //       let decimalAHora = this.decimal * 60;
            //       this.decimal2 = decimalAHora % 1;
            //       this.entero2 = decimalAHora - this.decimal2;
          
            //       this.cantidadHoras = this.cantidadHoras + this.entero;
            //       this.temporalMinutos = this.entero2 / 100
            //       console.log("-->>"+this.cantidadHoras + ":" + this.entero2)
            //       this.temporalPromedio = this.temporalMinutos /this.cantidadActividades
            //       this.promedioHoras = (this.cantidadHoras / this.cantidadActividades) + this.temporalPromedio
                  
            //       var indice = new Date(this.año, this.mes - 1, dia).getDay();
            //       this.diass.push(this.diasSemana[indice])
            //       this.fechass.push(dia)
            //       if(isNaN(this.promedioHoras)){
            //         this.promedios.push(0)
            //       }else{
            //       this.promedios.push(this.promedioHoras)
            //       }
            //       console.log(`El día número ${dia} del mes ${this.mes} del año ${this.año} es ${this.diasSemana[indice]}`);

            //   }
            // })
            
            this.actividadService.getAllByPersonalId(response.persona.cuil).subscribe(
                respon=>{
                  this.actividadList = respon
                  this.mesHoy = this.fechaHoy.toLocaleDateString().toString().substring(3, 5)

                  this.actividadList.forEach(respo=>{
                    if ((respo.fecha.toString().substring(5, 7)) == this.mesHoy){
                        this.contadorActividades.push(respo)
                        this.cantidadActividades = this.cantidadActividades + 1
                        this.cantidadHoras = this.cantidadHoras + respo.duracionHora
                        this.cantidadMinutos = this.cantidadMinutos + respo.duracionMinuto
                    }
                  })
                  this.cantidadMinutos = this.cantidadMinutos / 60;
                  this.decimal = (this.cantidadMinutos % 1);
                  this.entero = this.cantidadMinutos - this.decimal;
                  let decimalAHora = this.decimal * 60;
                  this.decimal2 = decimalAHora % 1;
                  this.entero2 = decimalAHora - this.decimal2;
                  this.cantidadHoras = this.cantidadHoras + this.entero;

                  this.contadorActividades.forEach(re=>{
                    this.fechasActividadesFilter.push(re.fecha)
                  })

                  this.fechasUnicas = this.fechasActividadesFilter.filter((valor, indice) => {
                    return this.fechasActividadesFilter.indexOf(valor) === indice;
                  })
                  this.cantidadFechas = this.fechasUnicas.length;
                  this.cantidadHorasPromedio = this.cantidadHoras / this.cantidadFechas;
                  this.entero2Promedio = this.entero2 / this.cantidadFechas;
                  this.entero2Promedio = this.entero2Promedio / 60;
                  this.decimal5 = (this.entero2Promedio % 1);
                  this.entero5 = this.entero2Promedio - this.decimal5;
                  let decimalAHora5 = this.decimal5 * 60;
                  this.decimal25 = decimalAHora5 % 1;
                  this.entero25 = decimalAHora5 - this.decimal25;
                  this.cantidadHorasPromedio = this.cantidadHorasPromedio + this.entero5;
                  let dec = this.entero25 /100
                  this.cantidadHorasPromedio = this.cantidadHorasPromedio + dec
            })


          })
      }
    )
  }

  noNan(){
    this.promedios.forEach(res=>{
      if(isNaN(res)){
        res = 0;
      }
    })
  }

  pdfObj = null;
  generatePDF(){
    var docDefinition = {
      content: [],

      //pageOrientation: 'landscape',
      // pageMargins: [ 30, 30, 30, 30 ],

     
      styles: {
        header: {
          fontSize: 14,
          bold: true,
          alignment: 'center',
          decoration: 'underline',
          margin: [0, 10, 0, 0]
        },
        subheader: {
          fontSize: 12,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        subheader2: {
          fontSize: 9,
          margin: [0, 0, 0, 15]
        },
        story: {
          fontSize: 9,
          italic: true,
          // alignment: 'center',
          width: '50%',
          margin: [0, 20, 0, 20]
        },
        tableExample: {
          margin: [0, 15, 0, 0]
        },
      }
    };

    //titulo del pdf
    docDefinition.content.push(
      { text: 'Visitas a Escuelas y Promedio de Horas', style: 'header' },
    );



    //primera tabla
    docDefinition.content.push({
      style: 'tableExample',
      table: {
        headerRows: 1,
        widths: [130, 226, 130],
        body: [
          [
            { text: "CUE", bold: true, fontSize: 12, alignment: 'center' },
            { text: "NOMBRE DE LA ESCUELA", bold: true, fontSize: 12, alignment: 'center' },
            { text: "VISITÓ LA ESCUELA", bold: true, fontSize: 12, alignment: 'center' },
          ],
        ]
      }
    }
    )

    //CONTENIDO TABLA
    for (let i = 0; i < this.escuelas.length; i++) {
      this.escuela = this.escuelas[i];
      for (let j = 0; j < this.visitasList.length; j++) {
        this.visit = this.visitasList[j].toString();
        if (i == j) {
          docDefinition.content.push({
            table: {
                    widths: [130, 226, 130],
                    body: [
                      [{text: this.escuela.cue.toString()},{text: this.escuela.nombre.toString()},{text:this.visit.toString(), alignment:'left'},
                      ],
                    ]
                  },
                  fontSize: 9,
          }
          )
        }
      }
    }

    //segunda tabla
    docDefinition.content.push({
      style: 'tableExample',
      table: {
        headerRows: 1,
        widths: [162, 162, 162],
        body: [
          [
            { text: "DIA", bold: true, fontSize: 12, alignment: 'center' },
            { text: "FECHA", bold: true, fontSize: 12, alignment: 'center' },
            { text: "PROMEDIO DE HORAS", bold: true, fontSize: 12, alignment: 'center' },
          ],
        ]
      }
    }
    )

    //contenido de la segunda tabla
    for (let i = 0; i < this.diass.length; i++) {
      this.diasSS = this.diass[i];
      for (let j = 0; j < this.fechass.length; j++) {
        this.fechaSS = this.fechass[j].toString();
        for(let k = 0; k < this.promedios.length; k++){
          this.promedioSS = this.promedios[k].toString();
        if ((i == j) && (j == k)) {
          docDefinition.content.push({
            table: {
                    widths: [162, 162, 162],
                    body: [
                      [{text: this.diasSS.toString()},{text: this.fechaSS.toString() + "/" + this.mesHoy},{text:this.promedioSS.toString()+" HS", alignment:'left'},
                      ],
                    ]
                  },
                  fontSize: 9,
          }
          )
        }
      }
    }
    }

    this.pdfObj = pdfMake.createPdf(docDefinition).open();
  }

  // meses(){
  //   this.mes = Number(this.mesHoy)
  //   this.diasMes= new Date(this.año, this.mes, 0).getDate();
  //   this.diasSemana= ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];

  //   for (var dia = 1; dia <= this.diasMes; dia++) {
  //     // Ojo, hay que restarle 1 para obtener el mes correcto
  //     var indice = new Date(this.año, this.mes - 1, dia).getDay();
  //     this.diass.push(this.diasSemana[indice])
  //     this.fechass.push(dia)
  //     console.log(`El día número ${dia} del mes ${this.mes} del año ${this.año} es ${this.diasSemana[indice]}`);
  //   }
  // }

}
