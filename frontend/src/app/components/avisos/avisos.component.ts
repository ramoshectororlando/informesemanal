import { Component, OnInit } from '@angular/core';
import { Avisos } from 'src/app/model/avisos.model';
import { AvisosService } from 'src/app/services/avisos.service';

import _ from "lodash";

@Component({
  selector: 'app-avisos',
  templateUrl: './avisos.component.html',
  styleUrls: ['./avisos.component.css']
})
export class AvisosComponent implements OnInit {

  avisosList: Avisos[] = [];
  pageActual: number = 1;

  constructor(private avisosService: AvisosService
    ) { }

  ngOnInit(): void {
    this.traerAvisos();
    this.marcarMensajeLeido();
  }

  traerAvisos(){
    this.avisosService.getAll().subscribe(
      response=>{
        // this.avisosList = response
        response.forEach(x=>{
          if(x.eliminado == false){
            this.avisosList.push(x)
          }
        })
      }
    )
  }

  listaAvisosOrdenados() {
    return _.sortBy(this.avisosList, 'fecha').reverse();
  }

  marcarMensajeLeido(){
    this.avisosService.getAll().subscribe(
      respo=>{
        respo.forEach(res=>{
          res.leido = true;
          this.avisosService.update(res).subscribe(
            x=>{
              
            }
          )
        })
      }
    )
  }

  leido(){
    window.location.reload();
  }

}
