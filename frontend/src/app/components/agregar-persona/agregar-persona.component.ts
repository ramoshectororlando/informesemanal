import { PersonalService } from './../../services/personal.service';
import { Domicilio } from './../../model/domicilio.model';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonaService } from './../../services/persona.service';
import { Persona } from './../../model/persona.model';
import { Component, OnInit } from '@angular/core';
import { ValidacionService } from 'src/app/services/validacion.service';

@Component({
  selector: 'app-agregar-persona',
  templateUrl: './agregar-persona.component.html',
  styleUrls: ['./agregar-persona.component.css']
})
export class AgregarPersonaComponent implements OnInit {

  persona: Persona = new Persona();
  domicilio: Domicilio = new Domicilio();

  menu: boolean;
  dato: string;

  constructor(private personaService: PersonaService,
    private router: Router,
    private validacionService: ValidacionService,
    private personalService: PersonalService,
    private activatedRoute: ActivatedRoute) {
    this.findById(activatedRoute.snapshot.params['id'])
  }

  ngOnInit(): void {
    // this.cargarPersona();
    this.menuValidacion();
  }

  agregarPersona() {
    this.persona.domicilio = this.domicilio;
    this.personaService.add(this.persona).subscribe(
      response => {
        console.log("persona agregada");
      },
      err => {
        console.log("Error..." + err);
      },
      () => {
        this.router.navigate(["/listado-personas"])
      }
    )
  }


  findById(id) {
    this.personaService.get(id).subscribe(
      response => {
        this.persona = response;
        if (this.persona.domicilio != null)
          this.domicilio = this.persona.domicilio;
      },
      err => console.log(err)
    );
  }

  actualizarPersona() {
    this.personaService.update(this.persona).subscribe(
      response => {
        console.log("persona actualizada...")
        this.router.navigate(['/listado-personas'])
      }
    )
  }

  menuValidacion() {
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      response=>{
        if (response == null) {
          this.router.navigate(['/'])
        } else
          if (response.funcion == "ADMINISTRADOR") {
            this.menu = true;
          } else {
            this.menu = false;
          }
      }
    )
  }

}
