import { SpinnerService } from './../../services/spinner.service';
import { PersonalService } from './../../services/personal.service';
import { DomicilioService } from './../../services/domicilio.service';
import { Escuela } from './../../model/escuela.model';
import { EscuelaService } from './../../services/escuela.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidacionService } from 'src/app/services/validacion.service';

@Component({
  selector: 'app-listado-escuelas',
  templateUrl: './listado-escuelas.component.html',
  styleUrls: ['./listado-escuelas.component.css']
})
export class ListadoEscuelasComponent implements OnInit {

  escuelasList: Escuela[];

  menu: boolean;
  dato: string;

  pageActual: number = 1;

  filterPost= '';
  filterPostEscNom= '';

  constructor(
    private router: Router,
    private escuelaService: EscuelaService,
    private domicilioService: DomicilioService,
    private validacionService: ValidacionService,
    private personalService: PersonalService,
    private spinnerService: SpinnerService
  ) { }

  ngOnInit(): void {
    this.obtenerEscuelas();
    // this.menuValidacion();
  }

  irEscuela() {
    this.router.navigate(['listado-escuelas/agregar-escuela']);
  }

  obtenerEscuelas() {
    this.escuelaService.getAll().subscribe(
      response => {
        this.escuelasList = response;
      },
      err =>{
        console.log(err)
      }
    )
  }

  eliminarEscuela(escuela: Escuela) {
    this.escuelaService.delete(escuela.id).subscribe(
      re => this.domicilioService.delete(escuela.domicilio.id).subscribe(
        res => this.escuelaService.getAll().subscribe(
          response => this.escuelasList = response
        )
      )
    );
  }

  menuValidacion() {
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      response=>{
        if (response == null) {
          this.router.navigate(['/'])
        } else
          if (response.funcion == "ADMINISTRADOR") {
            this.menu = true;
          } else {
            this.menu = false;
          }
      }
    )
  }

}
