import { PersonalService } from './../../services/personal.service';
import { ValidacionService } from './../../services/validacion.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { PersonaService } from 'src/app/services/persona.service';

@Component({
  selector: 'app-validacion',
  templateUrl: './validacion.component.html',
  styleUrls: ['./validacion.component.css']
})
export class ValidacionComponent implements OnInit {

  user: string;
  pass: string;

  exist: boolean = false;
  exist2: boolean;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private validacionService: ValidacionService,
    private personaService: PersonaService,
    private personalService: PersonalService
  ) { }

  ngOnInit(): void {
  }

  personalExistente(cuil: string, cuil2: string) {
    if (this.user == null || this.pass == null) {
      this.toastr.info('Debe ingresar los campos requeridos', 'Atención')
    } else {
      if (cuil == cuil2) {
        this.personaService.getPersonaCuil(cuil).subscribe(
          r => {
            //this.exist = r
            // if (this.user == null || this.pass == null) {
            //   this.toastr.info('Debe ingresar los campos requeridos', 'Atención')
            // } else {
              if (r) {
                this.personalService.get(r.id).subscribe(
                  res => {
                    if ((this.user == this.pass) && ((res.funcion == "ADMINISTRADOR"))) {
                      this.router.navigate(['listado-informes-AdminAdmin']);
                      this.toastr.success('Validación correcta', 'Bienvenido')
                      sessionStorage.setItem('usuarioo', this.user)
                    } else if ((this.user == this.pass) && ((res.funcion == "RTE ESCUELA"))) {
                      this.router.navigate(['listado-informes']);
                      this.toastr.success('Validación correcta', 'Bienvenido')
                      sessionStorage.setItem('usuarioo', this.user)
                    } else if ((this.user == this.pass) && ((res.funcion == "FACILITADOR"))) {
                      this.router.navigate(['listado-informes-facilitador']);
                      this.toastr.success('Validación correcta', 'Bienvenido')
                      sessionStorage.setItem('usuarioo', this.user)
                    }
                  }
                )
              } else {
                this.toastr.error('Usuario o contraseña incorrectos', 'Validación incorrecta')
              }
            // }
          }
        )
      } else {
        this.toastr.error('Usuario o contraseña incorrectos', 'Validación incorrecta')
      }
    }
  }

}
