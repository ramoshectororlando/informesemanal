import { PersonalService } from './../../services/personal.service';
import { ActividadService } from './../../services/actividad.service';
import { Component, OnInit } from '@angular/core';
import { Actividad } from 'src/app/model/actividad.model';

import _ from "lodash";
import pdfMake from 'pdfmake/build/pdfmake';
import { Escuela } from 'src/app/model/escuela.model';

@Component({
  selector: 'app-listado-informes-facilitador',
  templateUrl: './listado-informes-facilitador.component.html',
  styleUrls: ['./listado-informes-facilitador.component.css']
})
export class ListadoInformesFacilitadorComponent implements OnInit {

  constructor(private actividadService: ActividadService,
              private personalService: PersonalService
              ) { }

  actividadList: Actividad[] = [];
  actividadListFilter: Actividad[] = [];

  dato: string;

  pageActual: number = 1;
  opcionSeleccionado: string = '';
  opcionSeleccionado2: string = '';
  opcionSeleccionado3: string = '';
  opcionSeleccionado4: string = '';
  nombrePersonal: String = '';
  apellidoPersonal: String = '';
  nombreEscuela: String = '';

  filterPost = '';
  filterPostInfEsc = '';

  persona: string;
  escuela: string;
  apellido: string;
  cuil: string;
  fecha: string;
  fee: Date;
  fee2: Date;

  cat1: number = 0;
  cat2: number = 0;
  cat3: number = 0;
  cat4: number = 0;
  cat5: number = 0;

  porcenCat1: number = 0;
  porcenCat2: number = 0;
  porcenCat3: number = 0;
  porcenCat4: number = 0;
  porcenCat5: number = 0;

  region1: number = 0;
  region2: number = 0;
  region3: number = 0;
  region4: number = 0;
  region5: number = 0;

  porcenReg1: number = 0;
  porcenReg2: number = 0;
  porcenReg3: number = 0;
  porcenReg4: number = 0;
  porcenReg5: number = 0;

  nivel1: number = 0;
  nivel2: number = 0;
  nivel3: number = 0;
  nivel4: number = 0;
  nivel5: number = 0;
  nivel6: number = 0;
  nivel7: number = 0;
  nivel8: number = 0;
  nivel9: number = 0;
  nivel10: number = 0;
  nivel11: number = 0;

  porcenNiv1: number = 0;
  porcenNiv2: number = 0;
  porcenNiv3: number = 0;
  porcenNiv4: number = 0;
  porcenNiv5: number = 0;
  porcenNiv6: number = 0;
  porcenNiv7: number = 0;
  porcenNiv8: number = 0;
  porcenNiv9: number = 0;
  porcenNiv10: number = 0;
  porcenNiv11: number = 0;

  escuelasList: Escuela[] = [];
  cueEscuelasList: string[] = [];
  cueEscuelasListFilter: string[] = [];
  nombreEscuelaList: string[] = [];
  nombreEscuelaListFilter: string[] = [];
  cantidadEscuelasList: number[] = [];
  contCantidad: number = 0;
  cantidadEscuelas: number;
  cantidadEsc: string;

  cantidadHorasT3: number = 0;
  cantidadMinutosT3: number = 0;
  decimalT3: number;
  enteroT3: number;
  decimal2T3: number;
  entero2T3: number;
  promedioHorasT3: number = 0;

  cantidadActividades2: number = 0;

  ngOnInit(): void {
    this.obtenerActividades();
    this.obtenerActividades2();
    // this.cabecera();
  }

  obtenerActividades() {
    // this.dato = sessionStorage.getItem('usuarioo')

    this.actividadService.getAll().subscribe(
      response => {
        this.actividadListFilter = response;
        this.actividadList = this.actividadListFilter;

        this.cabecera()
        // this.personalService.getPersonalCuil(this.dato).subscribe(
        //   respo=>{
        //     if(respo.funcion == "ADMINISTRADOR"){
        //       this.persona = respo.funcion.toString()
        //       this.apellido = ""
        //       this.cuil = "---"
        //     }else if(respo.funcion == "FACILITADOR"){
        //       this.persona = respo.funcion.toString()
        //       this.apellido = ""
        //       this.cuil = "---"
        //     }else{
        //       this.persona = respo.persona.nombres.toString()
        //       this.apellido = respo.persona.apellido.toString()
        //       this.cuil = respo.persona.cuil.toString()
        //     }
        //   }
        // )
      }
    )
  }

  listaActividadesOrdenada() {
    return _.sortBy(this.actividadList, 'fecha').reverse();
  }

  capturar() {
    this.actividadList = this.actividadListFilter.filter(x =>
      x.fecha.toString().substring(5, 7).toLowerCase().includes(this.opcionSeleccionado.toLowerCase()))
  }

  capturarRegion() {
    this.actividadList = this.actividadListFilter.filter(x =>
      x.escuela.region.id.toString().toLowerCase().includes(this.opcionSeleccionado2.toLowerCase()))
  }

  recargar() {
    window.location.reload();
  }

  filtrosCombinados() {
    if (this.nombreEscuela == '' && this.apellidoPersonal == '' && this.nombrePersonal == '' && this.opcionSeleccionado == '' 
    && this.opcionSeleccionado2 == '' && this.opcionSeleccionado3 == '' && this.opcionSeleccionado4 == '') {
      this.actividadList = this.actividadListFilter
      this.obtenerActividades2();
    } else {
      this.actividadList = this.actividadListFilter.filter(x =>
        (x.escuela.nombre.toString().toLowerCase().includes(this.nombreEscuela.toLowerCase()))
        && ((x.personal.persona.apellido).toString().toLowerCase().includes(this.apellidoPersonal.toLowerCase()))
        && ((x.personal.persona.nombres).toString().toLowerCase().includes(this.nombrePersonal.toLowerCase()))
        && (x.fecha.toString().substring(5, 7).toLowerCase().includes(this.opcionSeleccionado.toLowerCase()))
        && (x.escuela.region.id.toString().toLowerCase().includes(this.opcionSeleccionado2.toLowerCase()))
        && (x.categoria.categoriaCategoria.id.toString().toLowerCase().includes(this.opcionSeleccionado3.toLowerCase()))
        && (x.destinatario.id.toString().toLowerCase().includes(this.opcionSeleccionado4.toLowerCase()))
      )
      this.obtenerActividades2();
    }
  }

  cabecera(){
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      respo=>{
        if(respo.funcion == "ADMINISTRADOR"){
          this.persona = respo.funcion.toString()
          this.apellido = ""
          this.cuil = "---"
        }else if(respo.funcion == "FACILITADOR"){
          this.persona = respo.funcion.toString()
          this.apellido = ""
          this.cuil = "---"
        }else{
          this.persona = respo.persona.nombres.toString()
          this.apellido = respo.persona.apellido.toString()
          this.cuil = respo.persona.cuil.toString()
        }
      }
    )
  }

  pdfObj = null;
  generatePDF() {

    var docDefinition = {
      content: [],

      pageOrientation: 'landscape',
      // pageMargins: [ 50, 50, 200, 50 ],

      images: {
        mySuperImage: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBQSFBcUExUXFxcXFxwYGhoaGRkcIRoaIBoYHBwYGRoaICwkHB0qIRgdJDYkKS0vMzM0GiI4PjgyPSwyMy8BCwsLDw4PHhISHTIqIiE+MjIyOjIyMjI0NDovMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/AABEIAIkBbwMBIgACEQEDEQH/xAAcAAEAAwEBAQEBAAAAAAAAAAAABQYHBAMCAQj/xABJEAACAQICBQgGBwUHAwUBAAABAgADEQQGBQcSITETNEFRYXFzsiJygZGhsRQyM1KCwcIjNUKS0RVDU2J0s8ODk9IkJVSi8Bb/xAAZAQEAAwEBAAAAAAAAAAAAAAAAAQIEAwX/xAAmEQADAAIABQUBAAMAAAAAAAAAAQIDERIhMTIzEyJBUYEEI2Fx/9oADAMBAAIRAxEAPwDZoifkA/YmVZnzri8Ti1wWh29JSRUqhEYE9Iu6sqovS1t53CaFoTC16VJVxNc16vFn2EQX6lVFXd33MAk4iIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAJEZnx64bCV6zBWCU2bZbg27cp7+El5nmujH8no8UwRetVRO3ZW9QkdnoAfigEBq1zgtTGLhlweFw4rK3pUVKksilgDc7xZWmxT+ckw39n4zRtYggPToVW7SfRcD3kT+jZAEROfFYmnSUvVdEUcWdgqjvLGwkg6Inhh66VEV0ZXRhdWUhlYHgVI3Eds5cZprC0W2a2Io02+69VFPuYgwCRiedNwwDKQQQCCDcEHgQRxE9IAiRNTMmCVthsZhg17bJrUwb9Vi15JI4YAqQQRcEbwR1gwD0iIgFYzFnfB6PqCliGcMy7Q2UZhbhxEsOHqh0V14MoYdxFx85h2u3ntPwR8zNp0Nzej4VPyLIB2zxxFdKal3ZVUcWYgAdG8me0hM48zq9y+dZaVtpEU9Js6v7dwn/AMij/wBxP6wNN4UkAYikSTYDlE3nq4yi5Yy4mLpu7u6lX2bLs/dB6R2yeo5HpIysKtT0WDfw9Bv1TrURL02cZu6W0i3RPyfs4nc58TikpLtVHVFva7EKL9VzP3DYlKi7VN1deF1IIv3iV3P/ADUeIvyaeuROaj12/KX4Vw8Rz4/doskREodBERAEREAREQBERAEREA/J8qb8JH6dw9Sph6iUjZ2XdvtfrF+i43SHyTo2vQWpywKhiuypINiL7R3E2vce6WUpy3so6e0tFqnxUcAEk2AFyeyfcrebqtZkWhQR2NU2ZgrFVXqZgLC/ykStvRNPS2SGjdOYfEMVouWKi59BwAO9lAkpIvQWiVwtIIN5O9m62/oOiSkVrfLoFvXPqfsREgsIiIAmLa6cQa+MwmDXoS/4qtQIL9wp3/FNpmC4/F08RmEvWZVpU6wUljZdmmuyN/rC/eZDBKa69HcmmCqLuKq1LcNw2QrLb23901DLONFfB4eqP46SH22APxEo2tfSeExWjyKWIpO9OqlRVVwSd5Q2A6lqE9wMkNTeO5TR4pk3NKoyHsB9JR7iIBFa9qAOHwzniK5X2Gm5/SJTNG6NxmneSpUyEo4SjTpXYnYQhApYAfWdipPYLDvu+vXmmH/1H/FUkxqhwqpouiyjfVeo7nrYVGQH+Wmo9kEnBmzSz6F0XQw9Nwa5RaKOBuAUDbqAHpA4A9LDjaVzLWq4YzCDE4itUWrWBdf4rX4M5bexPEzm124rbxlKmeCUt/4mve3dLXg9aujqdNE/begir9megAflAK5qn0xVwuLqaNrE7JZ1Ck3CVUJDBOxrE7uq80jO+gW0hhGw6VOTZnU3ubWDDaDAfWFr7uu0xlNL062nUxWG2hTqYimy7Qsbsio5IPC7Fp/QtSoFBZiAALkncAOsmAZPi9TVMUW5LEOaoW67QUIW6jbeB2yL1NaeeliWwNQnk3DFFP8ABVX6yr1XF79q9suWltaujqDbKtUrEbiaSiw/E5UH2GZrkjFLW07TqoCFq4ivUANrhXSs4BsSL74B/REREkgwjXbz2n4I+Zm1aG5vR8Kn5FmK67ee0/BHzM2rQ3N6PhU/IsgHZITOPM6vcvnWTchM48zq9y+dZeO5Fb7WRerr7Gp4v6FlvmWaA0visOjLQpq6s1ySjtY2AtdWHQBJ3R2Yca9Wmj0VVGYBiKdQWHXcvYTrkx06bOOPIlKRdGsN56JSdI5xqNU5PCoGsbAkElu5R0SczfijTwlQg2LWQe02PwvInV/gVFN6xHpM2wD1KLXt3nyiViZUumi903SlERpzT7YjDmlWQ06quptYi4s3Qd46PfLNkTmg9dvynBrBwamklWw2g2wT1ggn8vjO/InNB67flL008W19lJTWTT+j6zPmBsGaYWmH5QPxYi2zsdQP3vhJbR2I5WklQi22oa3G1xwlP1j/AFqHdU/45atAc2o+GvylKlKE/kvNN219EPi80smLGH5NSDURNraN/SKi9rdG1LTM00r+9F/1FLzJNFxNYIjMeCqW9wvGSUta+RFNt7+CEzBmanhTsKNupa+zewUHhtH8pCUMzY4lSaA2GIA9BwN5t9a/xnFlTC/S8U9Wr6QW7m/SxPoj/wDdU0e0muGOWtsrPFfPekeGNxBp03qW2thS1h02F5TGzPjnG3Tw4Ccb7Dtu7DcX9kvbC/HhE5zSXVbOlS30eiq5bzV9IbkqqhHP1SODdYseBlrmY6WUUNInZ3AVEYW/zBSbe0maW7gAk8ALy+WUtNfJXHTe0/ghcwZip4TdbbqEXC8LDrY9AldXNOONmFAFDw/ZvY/iva3bOLQdH6fjWqVBtLc1CDwsDZEPZw9xmlWk1wxy1tlVxXzT0jk0hizSoPV2QSiF9m9gSBe15GZZ082M5TaphNjZ4MTe9+sDqnbmPmlfwn8pla1c/wB/+D9chSnDfyWqmrSLdj8YlBGqObKo9/UB2ymtm3F1SzYegNhePos5t2kEAHsn1rExZ/Z0gd1i5+Qv8ZaNA4MUcPTQCx2AW7WIux95kpTEJtbbIbdU0nrRH5bzKuKJpuuxUAvboYdJH9JYpmum1+iaQDruBZagHrbmHde80gG4lckpaa6MnHTe0+qKjSzmBUqpVphBT2wLMSWZW2QtrcTI+vm/Fi1TkVSmeG0r7x0WfcPhIzB4VaukWptvU16pI6wGc2+E0TS+GWpQqIw3bB+AuJ0pRLXLqc5dUnz6HjoDS6YuntL6LKbMvUf6GSsz7V1VPK1V6Cit7Q1h5jNBnLJKmmkdsdcUps5sfiBSpVKht6CM+/sBP5TAMgZYp6XxFc1mqKiryl6eyDtO+5SWVha1+jomta0MfyGjK56XApDvcgXEr+o3A7OErVv8Wtsj1UQAEfiZx7JzLnnj9T2ESlUNKtiTUWmxQM1IguFJUECmDa9uBEidROkLVMRQJPpotVR0XB2XPuKTZyLzAsssNH6f5I+iprVKNv8AJUu1Me804Bb9evNMP/qP+KpJ/VP+6cN/1f8AfqSA16n/ANJh/wDUf8VST+qc/wDtOG/6v+/UgGc66KGxj6VRt6vSU+xWsQZqWEypoypTSoMFhrOqsP2ScGAPV2yK1pZVfH4dXoi9aiSyr99D9ZB/m3Ajut0ykZU1nPgaQwuLou/JeipvsuoH8Dq2+44QDVaGU9HoyumDw6spBDCkgII4EEDcZnmu3T1RTSwVMkK6crVsfrgsVRD/AJbqxI6d3VLDlLWKmkcV9HSgyLybPtMwJuCN27cOMquvHRTirQxag7JTkWb7jKzOl/W22/l7RALrk3JGEwdCkzU0qViqu1RgGO0ReyX+qo6APjMyyyLZisNwGMxPlry44PWxhFwqlkfl1QLyQXcWAsLPw2fjKHkg1V03ROIBSq1Wo7hhskNUpVG4HhfbFh2iCT+i4iJJBhGu3ntPwR8zNq0Nzej4VPyLMV1289p+CPmZtWhub0fCp+RZAOyQmceZ1e5fOsm5CZx5nV7l86y8dyK32si9XX2NTxf0LLfKhq6+xqeL+hZb7y2XvZTF2oqusC/0ZfEF/c06cj8zp+s/nafeccMamFew3pZ/cd/wvI3V/jQ1JqJPpI20B1q1t47jf3iX64v+Mjpl/wCo6M/81Hir8nnrkXmg9dvynDrCxainTpXG0z7VuoAEX+PwndkTmg9dvyhr/F+kJ/5fwh9ZH1qHdU/45a9Ac2o+GvylY1j091BugFl9+wf0yw5Zrh8JSIPBAp7Cu4j4RXikT5GUrSv70X/UUvMkvGYr/Ra1uPJmULF4hamklZTdfpNMA9dnQH4iaViqIdGQ8GUr7xaMnLhGPnxFM1cW/b9foe705eZmWWMd9DxLU6voqboxP8JB3E9n9ZpPKrba2hbruLe+RnT4t/ZbC/br6OTS+kVw1Jqrb7bgB0seAlPwgx2kbuKvJU723XA7lA3tbrJndrEJ5Klbhtm/u3SXyoVOEpbNvqkH1rm/xhe2OJdWQ/dWvhFA0jgWw+LWm1Q1CGQ7RBF7lTwJPzmm6VvyFW3Hk3t37JmeZkrK+PJBuA6L7Rsg/Hd7JpjqGBB4EWk5W9S2VxJbpIoerm23V69hLe9r/lL/ADMNC4j+z8YUqbluabHsv6L93A+0zSlrKV2gwKkX2gRa3XfhIzL3b+y2F+3X0cWY+aV/CfymVnVz/f8A4P1yzZj5pX8J/KZWdXP9/wDg/XJnxUK8iI7WFf6QOrkxb3maOvDdKNrEwpvSqgbrFD38R+ctWgsWK2HpuDc7IDdjAWYe8SL5xLIjlbKXn/nSW/wl89SX/DfZr6o+QmeacP0rSARd4DKl/V3t7L3mjgWFhGTlMoY+dUzNtDfvM+NV+bzQ8b9m/qN5TM80N+8z41X5vNDx32b+o3lMnN3IYe1lD1d/bP4X6lmiTPNXX21Tw/1LNDlc3eTh7Ec2LwVOsAtWmlQA3AdVYA9dmB3z6wuGSkoSmioovZVUKBckmwG4XJJ9s6InI7CcD6Jw7PyjUKRqXDbZpoW2hazbRF7iw39k74gHLjMDSrALWppUANwHRWAPWAwNjPvDYZKShKaqii9lUBQLm5sBuG8k+2e8QBOHFaKw9U7VWhSqN1vTRj72E7ogHNhcJTpDZpU0pjqRVUe5RPrFYdKqFKiq6MLMrAEEdoMoWS9ZB0livo5wwpfs2faFUv8AVtutya9fXNEgFdwWS9HUXFSnhKaupuDYmx6wCSLyUOisOanKmjSNW9+U2E27gWB2rXvad0QBERAMI1289p+CPmZtWhub0fCp+RZiuu3ntPwR8zNq0Nzej4VPyLIB2SO05gDiKD0lYKWtvIvazA/lJGJKenshra0Z+cgVP8ZP5D/Wfh1fvb7Wn/If6zQYnX1r+zn6MHwyAix3gix7ZR8dk+slTbwlQKL3F2KlewEcRL3EpN1PQtUKupTKeT3am5q1Nqs4ADG7BbMCd53k2FpO5d0W2Fo8mzBztFrgW3G27fJaIrJVLTExMvaI7Tmi1xVI023HirfdYcDKhh8p4xSUWqEpt9bZdhcdqiaBEmclStIVjVPbKacl7NWm9KoFFMoTcEksrAk9QvaXKIlap11JmVPQgNPZZpYo7V9h7W2h09W0OnvkJh8jOrAtWGyGB3Kb7u82l6iWWWktbIeOW96ODTGjVxNJqbbr7weojgZUsNlnHUrpTrBUY7yGI9uzbce4y+RIm2lpCoTeylVclEOjU6oGyFLbQJLOGJLcdwO4eyXQT9iRVOupMyp6EJp3L1PF2YkpUAsGHV1MOkSvJkNr76y7N+hTf4m15fIlpyUlpMq8ct7aOLSOENWg9IGxdClyOsWuRIvLGgWwfKbThtvZ4Ai1r9ffLDEqraTXwyzlN7OTSWBTEU2p1BdW94PQR2ymjKuMolloVgEPGzMtx2gdMvsSZtzyRFQq6ldy3lpcKTUdtuoRa/Qo6QO3tliiJFU6e2TMqVpFVwGV2p4s4k1FILu+zsm/pFt179G1LLXp7SMvDaUj3i09Yh03zYmUlpFYyxlt8I7O1RWDJs2AI6Qb7+6WeIiqdPbEypWkfsREgsIiIAiIgHyTbeZ8pVVuDA9xBmK6zMz18Xiv7PwrNsq4pkISDUqtu2SRxUX4d958tkLH6JpjHYbEK70/Tq00VgNgb2FybVFtxuBu3iQCP1MfvIeDU+az+gJ/P+pn95Dwan6Zr+dcwro7CPXttPcJTX71Rr2HcACx7FMAnHqqvFgO8gT7DA7xvmF6AyTidNo+MxeJZdtiKd1272Nidkn0UvcAC17XvOrV5pzEYDHnRmLYlC5pAMxISoBtIULb9hxaw/zLANsiIkgwjXbz2n4I+Zm1aG5vR8Kn5FmK67ee0/BHzM2rQ3N6PhU/IsgHZIbNVZkwtRkYqwAsQbEXZRJmQeceZ1fw+dZeO5Fb7WZmcVVY/aVCSfvNx9896WkcTQO6pUQ9RJ+R4z40Pzmh41P/AHFmlZm0YuIoPuG0qllPSCN9vbNmS1LSa6mOJdJtPmiPytmb6SeSq2FQC4I3Bx3dBlqmK4TEGlUSovFGDe7omzUKgZVYcGAPvE4Z8al7XyaMNulp/BF5qJGEqkEg7I3jd/EszbRuIc1qV3f7RP4m++vbNJzZzOt6o8yzMtF/b0vFp+dZ0wdjOefvRson7PyQ+aNInD4d2X6xsqesen2AE+yZUtvRpb0tshcyZtNNjSw9iw3M53gHqUdJ7ZTK+kq1Q3epUY+sfynjhcO1V1Rd7OwUd5PE/OarovQNDDoAqKzW3swBJPXv4d01txiXTmZFx5X15FGyjiazYqmgqPs7yw2iQQBexvNPnOuEphgwRQw3AgAG3snRM2S1b2kaYjhWtlczu5XCkqSDtrvBt19UpuWK7nF0QXYjbO4sT/C3bLlnvmh9dfzlJyrzyj658rTRi8b/AEz5fIjWZlubMQ4xdQB2AuNwYjoHbNSng+FpsbsiE9ZUH8pwx3wPejvkjiWtmOfSqn+I/wDM39Y+lVP8R/5m/rJLNiBcVUCgAXG4Cw+qOgS15Fw6NhiWRWPKNvKg9C9c2VkUxxaMkw3XDs9sguWwzFiSeVbeST/CnXLPPOlTVRZVCjqAA+U9BMFPips2yuFaM2ztjmOKKqzAIqruJG87zw7xI/LuOdMTSLOxBcAgsTx3dJ7Zz45/pGKY8eUq2HcW2V+Fp86QXkcRUA/u6rW7g5K/C03zK4eH/Riqnxb/ANmyRPHD1A6Kw4MoPvE9p55vMtzdXcYyoA7AejuDEfwjtlqyC5bDMWJJ5VuJJ/hTrlRzjzyr+HyiWzV9zVvFbypNeXxL8MmPyv8AS1RETIaxERAEREATyrPsqzdSk+4T1nhihdHHWrD4GAYNqwpfStMcq2/YFaue8nYB39Rqi3sm91qSurKwurAqR1gixEwfU5UFLSjU24tRq0h6yvTc/CmZvkgk/n/Ux+8h4NT5rJ7XxjDtYSiCbBalUjoJ9BUNusen75Bamf3kPBqfNZJ696DDEYZ+hqLqO9XUnziAWjLefNGYbCUKJrWNOmqkBW4gb+iZ3nHTNCrpWli8I20u1Qcm1v2iPv3HsCzStC5A0TiMPSrfRT+0pq321fiQL8Kk7l1aaJBBGF3ggj9tX4jeP7yAXCfsRJIMI1289p+CPmZtWhub0fCp+RZiuu3ntPwR8zNq0Nzej4VPyLIB2SDzjzOr+HzrJyQeceZ1fw+dZeO5Fb7WZvofnNDxqf8AuLNhZbgg9ItMe0Pzmh41P/cWa/iKmwjN91SfcCZ3/o7kcP5+jMWcbz3ma7oB9rC0SemmnyEyFzxM2LQ9Hk6FJfu01HwEt/T0RX+bqzkzZzOt6o8yzMtF/b0vFp+dZpmbOZ1vVHmWZnov7el4tPzrGDsZOfvRsspmsap6FFOt2b+UAfrlzlK1jU/Qot0BmX3hT+kzhh8iO2XsZDZGo7WLU/cR2+S/qlx03mOnhHVHRmLLtXFuu3TKdkats4tR99HX5N+mWjNGgBib1S5Xk6bejs3va543nbLw+p7uhxx8Xp+3qeFPPFJmVRTf0iB0dJtLbMVwP2lPxE8wm1CUzY5jWi+G3W9lcz3zQ+uv5yk5V55R9c+Vpds981Prr+cpOVeeUfXPladMXif6c8vkRrUREyGsyjN/PKvevlEt2r/mp8VvKkqOb+eVe9fKJbtX/NT4reVZry+Jfhkx+V/paJwaaxXJYerU6VRiO+1h8SJ3ysZ8xOxhdnpqOo9g9I/FR75mhbpI029S2UzK9DbxdFegNtfygn5gT2zjR2MXU6m2W96i/wAQZ4Zc0kmFrco6swClQFtxNt+8jq+M9MzaVp4qqtSmrLZAh2rdBJHAnrm73epv40Yvbwa+dl/yrX5TCUj1Lsn8JK/lJiVDV5XvRqJ9ypf2Mo/NTLfMWRapo2Y3uUzKs488q/h8ols1e81bxW8qSp5x55V/D5RLZq95q3it5UmjL4l+GfH5X+lqiImQ1iIiAIiIAiIgH895kw9TQ2lxiFUlOUNan0baPcVEv12Zh7RLrmLWhhHwrJg+UqV6q8mqFHXYLDZuSRYkX3BSbm0vmm9CYfG0+TxNJai3uL8VPWrDeD3SK0LkXR+DqCrRo+mPqs7M5XtXbJse6QDKdUFJqelTTcWZKdVGHUylQw94mlazsstpDCfshetRblKY+8LWenfouN/eqyfwugMLSrNiKdCmlZyxaoFAZixuxJ6bnfJWAYfkbWP9Ap/Q8ZTqFaZIRkA203/ZujEbh0Edgt0y6aG1mYXGYunhaNKreptem4RQCqltwDEncDxtLLpHLeDxLbdfDUqjdbICffPvRugMJhjehh6VM9aoAffAJSIiSDCNdvPafgj5mbTobm9Hwk8izJ9bmgMXicXTehh6lRRSALItwDfhNZ0VTK0KSsLFaaAg9BCgESAdkg848zq/h86yckVmPBvXwz00ALNs2BNuDKePslpeqRWucsy/RTBa9FmIAFWmSTwADqST2S65rzHT5JqVFw7OLEqbhV6d46eiVtsp4z/Cv28pT/8AKdeEyViXP7QpTHftH2Bd3xm2njbVN9DJPGk0l1InQOj2xFdEA3XDMepRx/pNdUWFhI7Q2hqeFTZQXJ+sx4n+g7JJzNlycb5dDRijhXPqQ2bOZ1vVHmWZnov7el4tPzrNRzDhXq4aolMbTMoAFwL7weJIEomAyzi0q02aiQq1FYnbp7gGBJ3P1CdMNJS9s5ZpbpaRp8h80aNOJwzov11s6esvR7Rce2TAiZ09PaNLW1pmK4bEPSqK6bmRgR3joPyM0M5ow1ag93COyMNhuO0VO4Hp39M+NP5SWuxqUiEqHiD9Vj17t4PbKtUyljFNhSDdodLf/ZgZrbx5NNvTMiWTHtJbRE4H7Sn66eYTaZm+j8n4nbRnCIFYMbtc7jewC36uuaPOX9FKmtM6YJcp7RXc9c0Prr+cpOVeeUfXPlaXzNmCqV8OUpLtNtKbXUbhe+9iBKvl/LuKpYmm70iqK1ydumbDZI4BiemWx0ljab+yuSW7TSNGiImY1GUZv55V718olu1f81PiN5UkLmTL+Kq4mo9OkWRiLHbpi/ogcGYGWLJ+AqUKBSquw22zWup3ELY3UkdBmnJSeNLZliWsjeiwSg6w8Td6VP7qlj3k2/KX6Z/mbQmLr4hnSkSu4KdumLgDqLXnPDpVtnXNvh0jhyzl0YtXZnZApAFgDfdfpn1mXLYwiI6uzhm2TcAW3Ejh3S35S0c+HobNRdlyxJFwe7epIn1mvR74jDlKa7ThlZRcDgd+9iBwnT1n6nXkc/SXB05lW1fV9mu6ffS/tU/0YzRJnmXNB4uhiEqPSIUEhjt0zYEEE2DXPGaHOefTvaZfAmp0zKs488q/h8ok9kvS1Cjh2WpUVGNRjYno2V3/AAM5cy5fxVbEvUp0tpGtY7SC/ogcGYGRX/8AK43/AAD/ANyn/wCc0bioUtnH3zbaRolDTmGdgiVUZmNgAeJklM40HlzF08RTqPSKqrAk7dM2HcGvNHmbJMy/a9miKqlzWj9iInM6CIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAf/Z',
        //mySuperImage:'data:image/jpeg;base64,./../../assets/logo.png'
      },

      footer: function (currentPage, pageCount) {
        if(currentPage < pageCount){
        return [
          {
            text: currentPage.toString() + ' de ' + pageCount,
            alignment: 'right',
            margin: [0, 0, 20, 0]
          }
        ]
      } else if(currentPage == pageCount){
        return [
          {
            text: '__________________',
            alignment: 'center',
            margin: [500, 0, 0, 0],
            absolutePosition: { x: 500, y: -5 },
          },
          {
            text: 'Firma y Aclaración',
            alignment: 'center',
            margin: [500, 0, 0, 0],
            absolutePosition: { x: 500, y: 10 },
          },
          {
            text: currentPage.toString() + ' de ' + pageCount,
            alignment: 'center',
            margin: [0, 0, 20, 0],
            absolutePosition: { x: 770, y: 10 },
          }
        ]
      }
      },


      styles: {
        header: {
          fontSize: 14,
          bold: true,
          alignment: 'center',
          decoration: 'underline',
          margin: [0, 10, 0, 0]
        },
        subheader: {
          fontSize: 12,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        subheader2: {
          fontSize: 9,
          margin: [0, 0, 0, 15]
        },
        story: {
          fontSize: 9,
          italic: true,
          // alignment: 'center',
          width: '50%',
          margin: [0, 20, 0, 20]
        },
        tableExample: {
          margin: [0, 15, 0, 0]
        },
      }
    };

    docDefinition.content.push(
      {
        image: 'mySuperImage',
        width: 120,
        alignment: 'center',
        margin: [0, -25, 0, 0]
      }
    )

    docDefinition.content.push(
      { text: 'LISTADO DE INFORMES', style: 'header' },
    );

    docDefinition.content.push({
      columns: [
        {
          text: 'Referente Técnico:', style: 'subheader',
          width: '*',
        }, {
          text: 'Cuil:', style: 'subheader',
          width: '*'
        }, {
          text: 'Fecha de Generación:', style: 'subheader',
          width: '*'
        }],
      columnGap: 20
    }
    )

    docDefinition.content.push({
      columns: [
        {
          text: this.persona + " " + this.apellido, style: 'subheader2',
          width: '*',
        }, {
          text: this.cuil, style: 'subheader2',
          width: '*'
        }, {
          text: new Date().toLocaleDateString() + " - " + new Date().toLocaleTimeString(), style: 'subheader2',
          width: '*'
        }],
      columnGap: 20
    }
    )

    docDefinition.content.push({
      table: {
        headerRows: 1,
        widths: [50, 48, 120, 252, 98, 35, 50, 47],
        body: [
          [
            { text: "CUE", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Fecha", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Institución", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Actividad", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Categoría", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Cant. Part.", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Duración", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Carga", bold: true, fontSize: 12, alignment: 'center' },
          ],
        ]
      }
    }
    )

    this.listaActividadesOrdenada().forEach(actividadess => {
      this.fee = new Date(actividadess.fecha.toString()),
        this.fee.setDate(this.fee.getDate() + 1),

        this.fee2 = new Date(actividadess.fechaCarga.toString()),
        this.fee2.setDate(this.fee2.getDate() + 1),

        docDefinition.content.push({

          table: {
            widths: [50, 48, 120, 252, 98, 35, 50,47],
            body: [

              [actividadess.escuela.cue.toString(),
              this.fee.toLocaleDateString(),
              actividadess.escuela.nombre.toString(),
              actividadess.detalle.toString(),
              actividadess.categoria.categoria.toString(),
              actividadess.cantidadParticipantes.toString(),
              actividadess.duracionHora.toString() + ":" + actividadess.duracionMinuto.toString() + " hs.",
              this.fee2.toLocaleDateString() + " " + actividadess.hora.toString(),
              ],
            ],
            margin: [0, 0, 0, 50]
          },
          fontSize: 9,
          
        }
        )
    })

    //espacio
    docDefinition.content.push(
      { text: '', style: 'header', pageBreak: 'after'},
    );

    //--------------------------------------------------
    //SEGUNDO PDF
    //titulo del pdf
    docDefinition.content.push(
      { text: 'Resumen Estadístico', style: 'header' },
    );

    //cabecera
    // docDefinition.content.push({
    //   columns: [
    //     {
    //       text: 'Referente Técnico:', style: 'subheader',
    //       width: '*',
    //     }, {
    //       text: 'Cuil:', style: 'subheader',
    //       width: '*'
    //     }, {
    //       text: 'Fecha de Generación:', style: 'subheader',
    //       width: '*'
    //     }],
    //   columnGap: 20
    // }
    // )

    //datos para la cabecera
    // docDefinition.content.push({
    //   columns: [
    //     {
    //       text: this.persona + " " + this.apellido, style: 'subheader2',
    //       width: '*',
    //     }, {
    //       text: this.cuil, style: 'subheader2',
    //       width: '*'
    //     }, {
    //       // text: new Date().toLocaleDateString() + " - " + new Date().toLocaleTimeString(), style: 'subheader2',
    //       text: new Date().toLocaleDateString(), style: 'subheader2',
    //       width: '*'
    //     }],
    //   columnGap: 20
    // }
    // )

    //primera tabla
    docDefinition.content.push({
      style: 'tableExample',
      table: {
        headerRows: 1,
        widths: [230, 230, 230],
        body: [
          [
            { text: "Total de actividades cargadas", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Total de horas cargadas", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Total de CUE trabajados", bold: true, fontSize: 12, alignment: 'center' },
          ],
        ]
      }
    }
    )

    //contenido de primera tabla
    docDefinition.content.push({
      table: {
        widths: [230, 230, 230],
        body: [
          [this.cantidadActividades2.toString(),
          this.cantidadHorasT3.toString() + ':' + this.entero2T3.toString(),
          this.cantidadEscuelas.toString(),
          ],
        ]
      },
      fontSize: 9,
    }
    )

    //segunda tabla / titulo de la tabla
    docDefinition.content.push({
      style: 'tableExample',
      table: {
        headerRows: 1,
        widths: [706],
        body: [
          [
            { text: "Total de actividades cargadas por categoria", bold: true, fontSize: 12, alignment: 'center' }
          ],
        ]
      }
    }
    )

    //segunda tabla / encabezados de la tabla
    docDefinition.content.push({
      table: {
        headerRows: 1,
        widths: [348, 170, 170],
        body: [
          [
            { text: "Categorias de actividades", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Cantidad", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Porcentaje", bold: true, fontSize: 12, alignment: 'center' }
          ],
        ]
      }
    }
    )

    //segunda tabla / contenido de la tabla
    docDefinition.content.push({
      table: {
        widths: [348, 170, 170],
        body: [
          [{ text: "Capacitación/Tutoría/Asesoramiento", bold: true },
          this.cat1.toString(),
          this.porcenCat1.toFixed(2) + '%',
          ],
          [{ text: "Asistencia Técnica", bold: true },
          this.cat2.toString(),
          this.porcenCat2.toFixed(2) + '%',
          ],
          [{ text: "Acompañamiento TIC en prácticas educativas", bold: true },
          this.cat3.toString(),
          this.porcenCat3.toFixed(2) + '%',
          ],
          [{ text: "Asistencia administrativa", bold: true },
          this.cat4.toString(),
          this.porcenCat4.toFixed(2) + '%',
          ],
          [{ text: "Grupos de indagación", bold: true },
          this.cat5.toString(),
          this.porcenCat5.toFixed(2) + '%',
          ],
        ]
      },
      fontSize: 9,
    }
    )

    //tercera tabla / titulo de la tabla
    docDefinition.content.push({
      style: 'tableExample',
      table: {
        headerRows: 1,
        widths: [706],
        body: [
          [
            { text: "Porcentaje de regiones atendidas", bold: true, fontSize: 12, alignment: 'center' }
          ],
        ]
      }
    }
    )

    //tercera tabla / encabezados de la tabla
    docDefinition.content.push({
      table: {
        headerRows: 1,
        widths: [134, 134, 134, 134, 134],
        body: [
          [
            { text: "Region I", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Region II", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Region III", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Region IV", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Region V", bold: true, fontSize: 12, alignment: 'center' }
          ],
        ]
      }
    }
    )

    //tercera tabla / contenido de la tabla
    docDefinition.content.push({
      table: {
        widths: [134, 134, 134, 134, 134],
        body: [
          [this.porcenReg1.toFixed(2) + '%',
          this.porcenReg2.toFixed(2) + '%',
          this.porcenReg3.toFixed(2) + '%',
          this.porcenReg4.toFixed(2) + '%',
          this.porcenReg5.toFixed(2) + '%'
          ],
        ]
      },
      fontSize: 9,
    }
    )


    //cuarta tabla / titulo de la tabla
    docDefinition.content.push({
      style: 'tableExample',
      table: {
        headerRows: 1,
        widths: [701],
        body: [
          [
            { text: "Cantidad de Escuelas Asistidas", bold: true, fontSize: 12, alignment: 'center' }
          ],
        ]
      }
    }
    )

    //cuarta tabla / encabezados de la tabla
    docDefinition.content.push({
      table: {
        headerRows: 1,
        widths: [450, 242],
        body: [
          [
            { text: "Escuela", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Cantidad", bold: true, fontSize: 12, alignment: 'center' }
          ],
        ]
      }
    }
    )

    //cuarta tabla contenido
    for (let i = 0; i < this.nombreEscuelaListFilter.length; i++) {
      this.escuela = this.nombreEscuelaListFilter[i];
      for (let j = 0; j < this.cantidadEscuelasList.length; j++) {
        this.cantidadEsc = this.cantidadEscuelasList[j].toString();
        if (i == j) {
          docDefinition.content.push({
            table: {
                    widths: [450, 242],
                    body: [
                      [{text: this.escuela.toString()},{text:this.cantidadEsc.toString(), alignment:'left'},
                      ],
                    ]
                  },
                  fontSize: 9,
          }
          )
        }
      }
    }

    //quinta tabla / titulo de la tabla
    docDefinition.content.push({
      style: 'tableExample',
      table: {
        headerRows: 1,
        widths: [458],
        body: [
          [
            { text: "Porcentaje de niveles atendidos", bold: true, fontSize: 12, alignment: 'center' }
          ],
        ]
      }
    }
    )

    //quinta tabla / encabezados de la tabla
    docDefinition.content.push({
      table: {
        headerRows: 1,
        widths: [300, 150],
        body: [
          [
            { text: "Niveles", bold: true, fontSize: 12, alignment: 'center' },
            { text: "Porcentaje", bold: true, fontSize: 12, alignment: 'center' }
          ],
        ]
      }
    }
    )

    //quinta tabla / contenido de la tabla
    docDefinition.content.push({
      table: {
        widths: [300, 150],
        body: [
          [{ text: "Especial", bold: true },
          this.porcenNiv1.toFixed(2) + '%',
          ],
          [{ text: "Inicial", bold: true},
          this.porcenNiv2.toFixed(2) + '%',
            ],
            [{ text: "Inicial / Primario", bold: true},
            this.porcenNiv3.toFixed(2) + '%',
            ],
            [{ text: "Inicial / Primario / Especial", bold: true},
            this.porcenNiv4.toFixed(2) + '%',
            ],
            [{ text: "J. Maternal / Inicial", bold: true},
            this.porcenNiv5.toFixed(2) + '%',
            ],
            [{ text: "Primario", bold: true},
            this.porcenNiv6.toFixed(2) + '%',
            ],
            [{ text: "Primario / Especial", bold: true},
            this.porcenNiv7.toFixed(2) + '%',
            ],
            [{ text: "Secundario", bold: true},
            this.porcenNiv8.toFixed(2) + '%',
            ],
            [{ text: "Secundario / Polimodal", bold: true},
            this.porcenNiv9.toFixed(2) + '%',
            ],
            [{ text: "Superior", bold: true},
            this.porcenNiv10.toFixed(2) + '%',
            ],
            [{ text: "Adultos", bold: true},
            this.porcenNiv11.toFixed(2) + '%',
            ],
        ]
      },
      fontSize: 9,
    }
    )


    this.pdfObj = pdfMake.createPdf(docDefinition).open();
  }

  obtenerActividades2(){
    this.cantidadHorasT3 = 0;
    this.cantidadMinutosT3 = 0;
    this.cat1 = 0;
    this.cat2 = 0;
    this.cat3 = 0;
    this.cat4 = 0;
    this.cat5 = 0;
    this.porcenCat1 = 0;
    this.porcenCat2 = 0;
    this.porcenCat3 = 0;
    this.porcenCat4 = 0;
    this.porcenCat5 = 0;
    this.region1 = 0;
    this.region2 = 0;
    this.region3 = 0;
    this.region4 = 0;
    this.region5 = 0;
    this.porcenReg1 = 0;
    this.porcenReg2 = 0;
    this.porcenReg3 = 0;
    this.porcenReg4 = 0;
    this.porcenReg5 = 0;
    this.nivel1 = 0;
    this.nivel2 = 0;
    this.nivel3 = 0;
    this.nivel4 = 0;
    this.nivel5 = 0;
    this.nivel6 = 0;
    this.nivel7 = 0;
    this.nivel8 = 0;
    this.nivel9 = 0;
    this.nivel10 = 0;
    this.nivel11 = 0;
    this.porcenNiv1 = 0;
    this.porcenNiv2 = 0;
    this.porcenNiv3 = 0;
    this.porcenNiv4 = 0;
    this.porcenNiv5 = 0;
    this.porcenNiv6 = 0;
    this.porcenNiv7 = 0;
    this.porcenNiv8 = 0;
    this.porcenNiv9 = 0;
    this.porcenNiv10 = 0;
    this.porcenNiv11 = 0;
    this.escuelasList = [];
    this.cueEscuelasList = [];
    this.nombreEscuelaList = [];
    this.cantidadEscuelasList = [];
    this.cantidadActividades2 = 0;
    this.cueEscuelasListFilter = [];
    this.nombreEscuelaListFilter = [];
    this.cantidadEscuelas = 0;
    this.contCantidad = 0;

    // this.actividadService.getAllByPersonalId(this.dato).subscribe(
    //   respon=>{
    //     this.actividadList = respon
    //     this.mesHoy = this.fechaHoy.toLocaleDateString().toString().substring(3, 5)

        this.actividadList.forEach(respo=>{
          // if ((respo.fecha.toString().substring(5, 7)) == this.mesHoy){
            this.cantidadActividades2 = this.cantidadActividades2 + 1
            this.cantidadHorasT3 = this.cantidadHorasT3 + respo.duracionHora;
            this.cantidadMinutosT3 = this.cantidadMinutosT3 + respo.duracionMinuto;
  
            //cantidad de actividades por categoria
            if (respo.categoria.categoriaCategoria.id == 1) {
              this.cat1 = this.cat1 + 1;
            } else if (respo.categoria.categoriaCategoria.id == 2) {
              this.cat2 = this.cat2 + 1;
            } else if (respo.categoria.categoriaCategoria.id == 3) {
              this.cat3 = this.cat3 + 1;
            } else if (respo.categoria.categoriaCategoria.id == 4) {
              this.cat4 = this.cat4 + 1;
            } else if (respo.categoria.categoriaCategoria.id == 5) {
              this.cat5 = this.cat5 + 1;
            }
  
            //cantidad de regiones atendidas
            if (respo.escuela.region.id == 1) {
              this.region1 = this.region1 + 1;
            } else if (respo.escuela.region.id == 2) {
              this.region2 = this.region2 + 1;
            } else if (respo.escuela.region.id == 3) {
              this.region3 = this.region3 + 1;
            } else if (respo.escuela.region.id == 4) {
              this.region4 = this.region4 + 1;
            } else if (respo.escuela.region.id == 5) {
              this.region5 = this.region5 + 1;
            }
  
            //cantidad de niveles atendidos
            if (respo.escuela.nivelEducativo.id == 1) {
              this.nivel1 = this.nivel1 + 1;
            } else if (respo.escuela.nivelEducativo.id == 2) {
              this.nivel2 = this.nivel2 + 1;
            } else if (respo.escuela.nivelEducativo.id == 3) {
              this.nivel3 = this.nivel3 + 1;
            } else if (respo.escuela.nivelEducativo.id == 4) {
              this.nivel4 = this.nivel4 + 1;
            } else if (respo.escuela.nivelEducativo.id == 5) {
              this.nivel5 = this.nivel5 + 1;
            } else if (respo.escuela.nivelEducativo.id == 6) {
              this.nivel6 = this.nivel6 + 1;
            } else if (respo.escuela.nivelEducativo.id == 7) {
              this.nivel7 = this.nivel7 + 1;
            } else if (respo.escuela.nivelEducativo.id == 8) {
              this.nivel8 = this.nivel8 + 1;
            } else if (respo.escuela.nivelEducativo.id == 9) {
              this.nivel9 = this.nivel9 + 1;
            } else if (respo.escuela.nivelEducativo.id == 26) {
              this.nivel10 = this.nivel10 + 1;
            } else if (respo.escuela.nivelEducativo.id == 34) {
              this.nivel11 = this.nivel11 + 1;
            }
  
            //se carga una lista con todas las escuelas trabajadas
            this.escuelasList.push(respo.escuela)
            
          // }
        })

        //se carga una lista con los cue y nombre de las escuelas trabajadas
  this.escuelasList.forEach(x => {
    this.cueEscuelasList.push(x.cue)
    this.nombreEscuelaList.push(x.nombre)
  })

  //se filtran las escuelas repetidas x cue
  this.cueEscuelasListFilter = this.cueEscuelasList.filter((valor, indice) => {
    return this.cueEscuelasList.indexOf(valor) === indice;
  })

  //se filtran las escuelas repetidas x nombre
  this.nombreEscuelaListFilter = this.nombreEscuelaList.filter((valor, indice) => {
    return this.nombreEscuelaList.indexOf(valor) === indice;
  })

  //cantidad de escuelas unicas
  this.cantidadEscuelas = this.cueEscuelasListFilter.length;

  //cantidad por escuela trabajada
  this.cueEscuelasListFilter.forEach(x => {
    this.cueEscuelasList.forEach(y => {
      if (x == y) {
        this.contCantidad = this.contCantidad + 1;
      }
    })
    this.cantidadEscuelasList.push(this.contCantidad)
    this.contCantidad = 0
  })

  //porcentaje de actividades
  this.porcenCat1 = (this.cat1 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenCat1)){
    this.porcenCat1 = 0;
  }
  this.porcenCat2 = (this.cat2 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenCat2)){
    this.porcenCat2 = 0;
  }
  this.porcenCat3 = (this.cat3 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenCat3)){
    this.porcenCat3 = 0;
  }
  this.porcenCat4 = (this.cat4 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenCat4)){
    this.porcenCat4 = 0;
  }
  this.porcenCat5 = (this.cat5 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenCat5)){
    this.porcenCat5 = 0;
  }

  //porcentaje de regiones
  this.porcenReg1 = (this.region1 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenReg1)){
    this.porcenReg1 = 0;
  }
  this.porcenReg2 = (this.region2 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenReg2)){
    this.porcenReg2 = 0;
  }
  this.porcenReg3 = (this.region3 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenReg3)){
    this.porcenReg3 = 0;
  }
  this.porcenReg4 = (this.region4 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenReg4)){
    this.porcenReg4 = 0;
  }
  this.porcenReg5 = (this.region5 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenReg5)){
    this.porcenReg5 = 0;
  }

  //porcentaje de niveles
  this.porcenNiv1 = (this.nivel1 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv1)){
    this.porcenNiv1 = 0;
  }
  this.porcenNiv2 = (this.nivel2 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv2)){
    this.porcenNiv2 = 0;
  }
  this.porcenNiv3 = (this.nivel3 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv3)){
    this.porcenNiv3 = 0;
  }
  this.porcenNiv4 = (this.nivel4 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv4)){
    this.porcenNiv4 = 0;
  }
  this.porcenNiv5 = (this.nivel5 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv5)){
    this.porcenNiv5 = 0;
  }
  this.porcenNiv6 = (this.nivel6 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv6)){
    this.porcenNiv6 = 0;
  }
  this.porcenNiv7 = (this.nivel7 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv7)){
    this.porcenNiv7 = 0;
  }
  this.porcenNiv8 = (this.nivel8 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv8)){
    this.porcenNiv8 = 0;
  }
  this.porcenNiv9 = (this.nivel9 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv9)){
    this.porcenNiv9 = 0;
  }
  this.porcenNiv10 = (this.nivel10 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv10)){
    this.porcenNiv10 = 0;
  }
  this.porcenNiv11 = (this.nivel11 * 100) / this.cantidadActividades2;
  if(isNaN(this.porcenNiv11)){
    this.porcenNiv11 = 0;
  }

  //se calcula la cantidad de horas y minutos
  this.cantidadMinutosT3 = this.cantidadMinutosT3 / 60;
  this.decimalT3 = (this.cantidadMinutosT3 % 1);
  this.enteroT3 = this.cantidadMinutosT3 - this.decimalT3;
  let decimalAHoraT3 = this.decimalT3 * 60;
  this.decimal2T3 = decimalAHoraT3 % 1;
  this.entero2T3 = decimalAHoraT3 - this.decimal2T3;

  this.cantidadHorasT3 = this.cantidadHorasT3 + this.enteroT3;
        
  // })

}


}
