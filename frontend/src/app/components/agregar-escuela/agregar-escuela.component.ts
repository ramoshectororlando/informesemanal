import { PersonalService } from './../../services/personal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EscuelaService } from './../../services/escuela.service';
import { Domicilio } from './../../model/domicilio.model';
import { Escuela } from './../../model/escuela.model';
import { NivelEducativo } from './../../model/nivelEducativo.model';
import { NivelEducativoService } from './../../services/nivel-educativo.service';
import { Ambito } from './../../model/ambito.model';
import { AmbitoService } from './../../services/ambito.service';
import { Sector } from './../../model/sector.model';
import { SectorService } from './../../services/sector.service';
import { Region } from './../../model/region.model';
import { RegionService } from './../../services/region.service';
import { Component, OnInit } from '@angular/core';
import { ValidacionService } from 'src/app/services/validacion.service';

@Component({
  selector: 'app-agregar-escuela',
  templateUrl: './agregar-escuela.component.html',
  styleUrls: ['./agregar-escuela.component.css']
})
export class AgregarEscuelaComponent implements OnInit {

  escuela: Escuela = new Escuela();
  domicilio: Domicilio = new Domicilio();
  regionesList: Region[];
  region: Region = new Region();
  sectoresList: Sector[];
  sector: Sector = new Sector();
  ambitosList: Ambito[];
  ambito: Ambito = new Ambito();
  nivelesList: NivelEducativo[];
  nivel: NivelEducativo = new NivelEducativo();

  menu: boolean;
  dato: string;

  constructor(private regionService: RegionService,
    private sectorService: SectorService,
    private ambitoService: AmbitoService,
    private nivelEducativoService: NivelEducativoService,
    private escuelaService: EscuelaService,
    private router: Router,
    private validacionService: ValidacionService,
    private personalService: PersonalService,
    private activatedRoute: ActivatedRoute) {
    this.findById(activatedRoute.snapshot.params['id'])
  }

  ngOnInit(): void {
    this.traerRegiones();
    this.traerSectores();
    this.traerAmbitos();
    this.traerNiveles();
    this.menuValidacion();
  }

  traerRegiones() {
    this.regionService.getAll().subscribe(
      response => {
        this.regionesList = response
      }
    )
  }

  traerSectores() {
    this.sectorService.getAll().subscribe(
      response => {
        this.sectoresList = response
      }
    )
  }

  traerAmbitos() {
    this.ambitoService.getAll().subscribe(
      response => {
        this.ambitosList = response
      }
    )
  }

  traerNiveles() {
    this.nivelEducativoService.getAll().subscribe(
      response => {
        this.nivelesList = response
      }
    )
  }

  agregarEscuela() {
    this.escuela.domicilio = this.domicilio;
    this.escuela.region = this.region;
    this.escuela.sector = this.sector;
    this.escuela.ambito = this.ambito;
    this.escuela.nivelEducativo = this.nivel;
    this.escuela.persona = null;
    this.escuelaService.add(this.escuela).subscribe(
      response => {
        console.log("escuela agregada");
        this.router.navigate(["/listado-escuelas"])
      },
      // err => {
      //   console.log("Error..."+err);
      // },
      // ()=>{
      //   this.router.navigate(["/listado-escuelas"])
      // }
    )
  }

  findById(id) {
    this.escuelaService.get(id).subscribe(
      response => {
        this.escuela = response;
        if (this.escuela.domicilio != null) {
          this.domicilio = this.escuela.domicilio;
        }
        this.region = this.escuela.region;
        this.sector = this.escuela.sector;
        this.ambito = this.escuela.ambito;
        this.nivel = this.escuela.nivelEducativo;
      },
      err => console.log(err)
    );
  }

  actualizarEscuela() {
    this.escuelaService.update(this.escuela).subscribe(
      response => {
        console.log("escuela actualizada...")
        this.router.navigate(['/listado-escuelas'])
      }
    )
  }

  menuValidacion() {
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      response=>{
        if (response == null) {
          this.router.navigate(['/'])
        } else
          if (response.funcion == "ADMINISTRADOR") {
            this.menu = true;
          } else {
            this.menu = false;
          }
      }
    )
  }

}
