import { PersonalService } from './../../services/personal.service';
import { CategoriaService } from './../../services/categoria.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoriaCategoria } from './../../model/categoriaCategoria.model';
import { CategoriaCategoriaService } from './../../services/categoria-categoria.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  menu: boolean;
  dato: string;

  categoriaList: CategoriaCategoria[];
  categoria: CategoriaCategoria = new CategoriaCategoria();

  constructor(private router: Router,
    private categoriaService: CategoriaCategoriaService,
    private subCategoriaService: CategoriaService,
    private personalService: PersonalService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    // this.menuValidacion();
    this.obtenerCategorias();
  }

  obtenerCategorias() {
    this.categoriaService.getAll().subscribe(
      response => {
        this.categoriaList = response;
      }
    )
  }

  verModal(abrirModal) {
    this.modalService.open(abrirModal)
  }

  datosModal(modal) {
    let modalDato = (<HTMLInputElement>document.getElementById('categoria')).value;
    this.categoria.detalle = modalDato;
    this.categoriaService.add(this.categoria).subscribe(
      response => {
        console.log("categoria agregada");
      },
      err => {
        console.log("Error..." + err);
      },
    )
    modal.close();
    window.location.reload();
  }

  // agregarCategoria() {
  //   this.categoriaService.add(this.categoria).subscribe(
  //     response => {
  //       console.log("categoria agregada");
  //     },
  //     err => {
  //       console.log("Error..." + err);
  //     },
  //     () => {
  //       this.router.navigate(["/categorias"])
  //     }
  //   )
  // }

  eliminarCategoria(categoria: CategoriaCategoria) {
    this.subCategoriaService.getAll().subscribe(
      response => {
        response.forEach(aa => {
          if (aa.categoriaCategoria.id == categoria.id) {
            this.subCategoriaService.delete(aa.id).subscribe(
              re => console.log("ELIMINADAS....")
            )
          }
        },
          this.categoriaService.delete(categoria.id).subscribe(
            resp => this.categoriaService.getAll().subscribe(
              respo => this.categoriaList = respo
            )
          )
        )
      }
    )

  }

  modificarCategoria(id,abrirModalModif){
    this.modalService.open(abrirModalModif)

    this.categoriaService.get(id).subscribe(
      response => {
        this.categoria = response;
        (<HTMLInputElement>document.getElementById('categoria')).value = this.categoria.detalle;
      }
    )
  }

  datosModalUpdate(modal2){
    let modalDatoUpdate = (<HTMLInputElement>document.getElementById('categoria')).value;
    this.categoria.detalle = modalDatoUpdate;
    this.categoriaService.update(this.categoria).subscribe(
      respo => {
        console.log(" cateogria actualizada...")
      }
    )
    modal2.close();
    window.location.reload();
  }

  irSubCategoria(id) {
    sessionStorage.setItem('idCategoria', id)
    this.router.navigate(["/sub-categorias"])
  }

  menuValidacion() {
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      response=>{
        if (response == null) {
          this.router.navigate(['/'])
        } else
          if (response.funcion == "ADMINISTRADOR") {
            this.menu = true;
          } else {
            this.menu = false;
          }
      }
    )
  }

}
