import { PersonalService } from './../../services/personal.service';
import { DomicilioService } from './../../services/domicilio.service';
import { Persona } from './../../model/persona.model';
import { PersonaService } from './../../services/persona.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ValidacionService } from 'src/app/services/validacion.service';

@Component({
  selector: 'app-listado-personas',
  templateUrl: './listado-personas.component.html',
  styleUrls: ['./listado-personas.component.css']
})
export class ListadoPersonasComponent implements OnInit {

  personasList: Persona[];

  menu: boolean;
  dato: string;

  pageActual: number = 1;

  filterPost = '';
  filterPostNom = '';
  filterPostDepto = '';

  constructor(
    private router: Router,
    private personaService: PersonaService,
    private personalService: PersonalService,
    private domicilioService: DomicilioService,
    private validacionService: ValidacionService
  ) { }

  ngOnInit(): void {
    this.obtenerPersonas();
    // this.menuValidacion();
  }

  irPersona() {
    this.router.navigate(['listado-personas/agregar-persona'])
  }

  obtenerPersonas() {
    this.personaService.getAll().subscribe(
      response => {
        this.personasList = response;
      }
    )
  }

  eliminarPersona(persona: Persona) {
    this.personalService.getAll().subscribe(x => {
      x.forEach(res => {
        if (res.persona.id == persona.id) {
          this.personalService.delete(res.id).subscribe(
            respo => console.log("personal eliminado...")
          )
        }
      },
        this.personaService.delete(persona.id).subscribe(
          re => this.domicilioService.delete(persona.domicilio.id).subscribe(
            res => this.personaService.getAll().subscribe(
              response => this.personasList = response
            )
          )
        )
      )
    })
  }

  menuValidacion() {
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      response=>{
        if (response == null) {
          this.router.navigate(['/'])
        } else
          if (response.funcion == "ADMINISTRADOR") {
            this.menu = true;
          } else {
            this.menu = false;
          }
      }
    )
  }

  irPersonal() {
    this.router.navigate(['agregar-personal'])
  }

}
