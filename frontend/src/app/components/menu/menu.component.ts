import { Avisos } from 'src/app/model/avisos.model';
import { AvisosService } from './../../services/avisos.service';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  msjLeido: boolean = true;
  avisosList: Avisos[];

  constructor(
    private router: Router,
    private authService: AuthService,
    private avisosService: AvisosService
  ) { }

  ngOnInit(): void {
    this.traerAvisos();
  }

  salir() {
    // sessionStorage.removeItem('usuarioo');
    // this.router.navigate(['/'])
    this.authService.logoutUser();
  }

  traerAvisos(){
    this.avisosService.getAll().subscribe(
      resp=>{
        resp.forEach(res=>{
          if(res.leido == false){
            this.msjLeido = false;
          }
        })
      }
    )
  }

}
