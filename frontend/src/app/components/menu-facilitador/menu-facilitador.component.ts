import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-facilitador',
  templateUrl: './menu-facilitador.component.html',
  styleUrls: ['./menu-facilitador.component.css']
})
export class MenuFacilitadorComponent implements OnInit {

  constructor(private router: Router,
              private authService: AuthService
    ) { }

  ngOnInit(): void {
  }

  salir() {
    // sessionStorage.removeItem('usuarioo');
    // this.router.navigate(['/'])
    this.authService.logoutUser();
  }

}
