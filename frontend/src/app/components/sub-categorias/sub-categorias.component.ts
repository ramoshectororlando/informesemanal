import { PersonalService } from './../../services/personal.service';
import { CategoriaCategoria } from './../../model/categoriaCategoria.model';
import { Categoria } from './../../model/categoria.model';
import { CategoriaService } from './../../services/categoria.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sub-categorias',
  templateUrl: './sub-categorias.component.html',
  styleUrls: ['./sub-categorias.component.css']
})
export class SubCategoriasComponent implements OnInit {

  menu: boolean;
  dato:string;

  subCategoriaList: Categoria[]=[];
  subCategoria: Categoria = new Categoria();
  categoria: CategoriaCategoria = new CategoriaCategoria();

  constructor(private router: Router,
              private subCategoriaService: CategoriaService,
              private personalService: PersonalService,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    // this.menuValidacion();
    this.obetenrId();
    this.obtenerSubCategorias();
  }

  obetenrId(){
    this.dato = sessionStorage.getItem('idCategoria')
    //sessionStorage.removeItem('idCategoria');
    
  }

  obtenerSubCategorias() {
    this.subCategoriaService.getAll().subscribe(
      response => {
        response.forEach(x=>{
          if(x.categoriaCategoria.id.toString() == this.dato){
            this.subCategoriaList.push(x);
          }
        })
      }
    )
  }

  verModal(abrirModal){
    this.modalService.open(abrirModal)
  }

  datosModal(modal){
    let modalDato = (<HTMLInputElement>document.getElementById('subCategoria')).value;
    this.subCategoria.categoria = modalDato;
    this.categoria.id = Number(this.dato);
    this.subCategoria.categoriaCategoria = this.categoria; 
    this.subCategoriaService.add(this.subCategoria).subscribe(
      response => {
        console.log("suCategoria agregada");
      },
      err => {
        console.log("Error..." + err);
      },
    )
    modal.close();
    window.location.reload();
  }

  eliminarSubCategoria(subCategoria: Categoria){
    this.subCategoriaService.delete(subCategoria.id).subscribe(
      response => this.subCategoriaService.getAll().subscribe(
        res => {
          this.subCategoriaList = [];
          res.forEach(x=>{
            if(x.categoriaCategoria.id.toString() == this.dato){
              this.subCategoriaList.push(x);
            }
          })
        }
      )
    );
  }

  modificarSubCategoria(id,abrirModalModif){
    this.modalService.open(abrirModalModif)

    this.subCategoriaService.get(id).subscribe(
      response => {
        this.subCategoria = response;
        (<HTMLInputElement>document.getElementById('subCategoria')).value = this.subCategoria.categoria;
      }
    )
  }

  datosModalUpdate(modal2){
    let modalDatoUpdate = (<HTMLInputElement>document.getElementById('subCategoria')).value;
    this.subCategoria.categoria = modalDatoUpdate;
    this.subCategoriaService.update(this.subCategoria).subscribe(
      respo => {
        console.log(" Sub-cateogria actualizada...")
      }
    )
    modal2.close();
    window.location.reload();
  }

  menuValidacion() {
    this.dato = sessionStorage.getItem('usuarioo')
    this.personalService.getPersonalCuil(this.dato).subscribe(
      response=>{
        if (response == null) {
          this.router.navigate(['/'])
        } else
          if (response.funcion == "ADMINISTRADOR") {
            this.menu = true;
          } else {
            this.menu = false;
          }
      }
    )
  }

}
