import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSearchPerson'
})
export class FilterSearchPersonPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultPost = [];
    for(const post of value){
      if(post.cuil.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        resultPost.push(post);
      };
    };
    return resultPost;
  }

}
