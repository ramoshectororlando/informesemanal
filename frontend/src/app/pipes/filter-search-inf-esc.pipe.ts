import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSearchInfEsc'
})
export class FilterSearchInfEscPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultPost = [];
    for(const post of value){
      if(post.escuela.nombre.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        resultPost.push(post);
      };
    };
    return resultPost;
  }

}
