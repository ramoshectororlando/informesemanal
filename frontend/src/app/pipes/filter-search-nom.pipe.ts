import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSearchNom'
})
export class FilterSearchNomPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultPost = [];
    for(const post of value){
      if((post.nombres.toLowerCase() + ' ' +
          post.apellido.toLowerCase()).indexOf(arg.toLowerCase()) > -1){
        resultPost.push(post);
      };
    };
    return resultPost;
  }

}
