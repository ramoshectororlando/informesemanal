import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSearchInform'
})
export class FilterSearchInformPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultPost = [];
    for(const post of value){
      if((post.personal.persona.nombres.toLowerCase() + ' ' +
          post.personal.persona.apellido.toLowerCase()).indexOf(arg.toLowerCase()) > -1){
        resultPost.push(post);
      };
    };
    return resultPost;
  }

}
