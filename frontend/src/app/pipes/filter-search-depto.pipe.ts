import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSearchDepto'
})
export class FilterSearchDeptoPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultPost = [];
    for(const post of value){
      if(post.domicilio.departamento(arg.toLowerCase()) > -1){
        resultPost.push(post);
      };
    };
    return resultPost;
  }

}
