import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSearch'
})
export class FilterSearchPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const resultPost = [];
    for(const post of value){
      if(post.cue.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        resultPost.push(post);
      };
    };
    return resultPost;
  }

}
