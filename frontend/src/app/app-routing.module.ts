import { ConsultasRteFacilitadorComponent } from './components/consultas-rte-facilitador/consultas-rte-facilitador.component';
import { ConsultasRteComponent } from './components/consultas-rte/consultas-rte.component';
import { EstadisticasFacilitadorComponent } from './components/estadisticas-facilitador/estadisticas-facilitador.component';
import { EstadisticasAdminComponent } from './components/estadisticas-admin/estadisticas-admin.component';
import { EstadisticasComponent } from './components/estadisticas/estadisticas.component';
import { AvisosFacilitadorComponent } from './components/avisos-facilitador/avisos-facilitador.component';
import { AvisosAdminComponent } from './components/avisos-admin/avisos-admin.component';
import { AvisosComponent } from './components/avisos/avisos.component';
import { InformeAdminComponent } from './components/informe-admin/informe-admin.component';
import { ListadoInformesAdminComponent } from './components/listado-informes-admin/listado-informes-admin.component';
import { AuthGuard } from './guards/auth.guard';
import { ListadoInformesFacilitadorComponent } from './components/listado-informes-facilitador/listado-informes-facilitador.component';
import { AgregarPersonalComponent } from './components/agregar-personal/agregar-personal.component';
import { SubCategoriasComponent } from './components/sub-categorias/sub-categorias.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { ValidacionComponent } from './components/validacion/validacion.component';
import { ListadoInformesComponent } from './components/listado-informes/listado-informes.component';
import { InformeComponent } from './components/informe/informe.component';
import { AgregarPersonaComponent } from './components/agregar-persona/agregar-persona.component';
import { AgregarEscuelaComponent } from './components/agregar-escuela/agregar-escuela.component';
import { ListadoPersonasComponent } from './components/listado-personas/listado-personas.component';
import { ListadoEscuelasComponent } from './components/listado-escuelas/listado-escuelas.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  // {path: '', component: AppComponent},
  {path: '', component: ListadoInformesComponent, canActivate: [AuthGuard]},
  {path: 'login', component: ValidacionComponent},
  {path: 'listado-escuelas', component: ListadoEscuelasComponent, canActivate: [AuthGuard]},
  {path: 'listado-escuelas/agregar-escuela', component: AgregarEscuelaComponent, canActivate: [AuthGuard]},
  {path: 'listado-escuelas/agregar-escuela/:id', component: AgregarEscuelaComponent, canActivate: [AuthGuard]},
  {path: 'listado-personas', component: ListadoPersonasComponent, canActivate: [AuthGuard]},
  {path: 'listado-personas/agregar-persona', component: AgregarPersonaComponent, canActivate: [AuthGuard]},
  {path: 'listado-personas/agregar-persona/:id', component: AgregarPersonaComponent, canActivate: [AuthGuard]},
  {path: 'listado-informes/agregar-informe', component: InformeComponent, canActivate: [AuthGuard]},
  {path: 'listado-informes/agregar-informe/:id', component: InformeComponent, canActivate: [AuthGuard]},
  {path: 'listado-informes', component: ListadoInformesComponent, canActivate: [AuthGuard]},
  {path: 'categorias', component: CategoriasComponent, canActivate: [AuthGuard]},
  {path: 'sub-categorias', component: SubCategoriasComponent, canActivate: [AuthGuard]},
  {path: 'agregar-personal', component: AgregarPersonalComponent, canActivate: [AuthGuard]},
  {path: 'listado-informes-facilitador', component: ListadoInformesFacilitadorComponent, canActivate: [AuthGuard]},
  {path: 'listado-informes-AdminAdmin', component: ListadoInformesAdminComponent, canActivate: [AuthGuard]},
  {path: 'listado-informes-AdminAdmin/agregar-informe-admin', component: InformeAdminComponent, canActivate: [AuthGuard]},
  {path: 'listado-informes-AdminAdmin/agregar-informe-admin/:id', component: InformeAdminComponent, canActivate: [AuthGuard]},
  {path: 'avisos', component: AvisosComponent, canActivate: [AuthGuard]},
  {path: 'avisos-AdminAdmin', component: AvisosAdminComponent, canActivate: [AuthGuard]},
  {path: 'avisos-facilitador', component: AvisosFacilitadorComponent, canActivate: [AuthGuard]},
  {path: 'estadisticas', component: EstadisticasComponent, canActivate: [AuthGuard]},
  {path: 'estadisticas-AdminAdmin', component: EstadisticasAdminComponent, canActivate: [AuthGuard]},
  {path: 'estadisticas-facilitador', component: EstadisticasFacilitadorComponent, canActivate: [AuthGuard]},
  {path: 'consultas-rte', component: ConsultasRteComponent, canActivate: [AuthGuard]},
  {path: 'consultas-rte-facilitador', component: ConsultasRteFacilitadorComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
