import { Persona } from './../model/persona.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  private url = environment.baseUrl + '/persona';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Persona[]> {
  return this.http.get<Persona[]>(this.url);
  }

  get(id: number):Observable<Persona> {
    return this.http.get<Persona>(this.url + '?id=' + id);
  }

  add(persona: Persona):Observable<Persona>{
    return this.http.post<Persona>(this.url, persona);
  }

  update(persona: Persona):Observable<Persona>{
    return this.http.put<Persona>(this.url, persona);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }

  // isExistPersona(cuil: string): Observable<boolean>{
  //   return this.http.get<boolean>(this.url + "/cuil?cuil=" +cuil);
  // }

  getPersonaCuil(cuil: string): Observable<Persona>{
    return this.http.get<Persona>(this.url + "/cuil?cuil=" +cuil);
  }

}
