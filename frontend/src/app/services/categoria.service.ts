import { environment } from './../../environments/environment';
import { Categoria } from './../model/categoria.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private url = environment.baseUrl + '/categoria';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Categoria[]> {
  return this.http.get<Categoria[]>(this.url);
  }

  get(id: number):Observable<Categoria> {
    return this.http.get<Categoria>(this.url + '?id=' + id);
  }

  add(categoria: Categoria):Observable<Categoria>{
    return this.http.post<Categoria>(this.url, categoria);
  }

  update(categoria: Categoria):Observable<Categoria>{
    return this.http.put<Categoria>(this.url, categoria);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
