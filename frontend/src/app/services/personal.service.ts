import { environment } from './../../environments/environment';
import { Personal } from './../model/personal.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {

  private url = environment.baseUrl + '/personal';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Personal[]> {
  return this.http.get<Personal[]>(this.url);
  }

  get(id: number):Observable<Personal> {
    return this.http.get<Personal>(this.url + '?id=' + id);
  }

  add(personal: Personal):Observable<Personal>{
    return this.http.post<Personal>(this.url, personal);
  }

  update(personal: Personal):Observable<Personal>{
    return this.http.put<Personal>(this.url, personal);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }

  getPersonalCuil(cuil: string): Observable<Personal>{
    return this.http.get<Personal>(this.url + "/cuil?cuil=" +cuil);
  }
  
}
