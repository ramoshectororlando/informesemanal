import { environment } from './../../environments/environment';
import { Sector } from './../model/sector.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SectorService {

  private url = environment.baseUrl + '/sector';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Sector[]> {
  return this.http.get<Sector[]>(this.url);
  }

  get(id: number):Observable<Sector> {
    return this.http.get<Sector>(this.url + '?id=' + id);
  }

  add(sector: Sector):Observable<Sector>{
    return this.http.post<Sector>(this.url, sector);
  }

  update(sector: Sector):Observable<Sector>{
    return this.http.put<Sector>(this.url, sector);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
