import { environment } from './../../environments/environment';
import { Region } from './../model/region.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  private url = environment.baseUrl + '/region';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Region[]> {
  return this.http.get<Region[]>(this.url);
  }

  get(id: number):Observable<Region> {
    return this.http.get<Region>(this.url + '?id=' + id);
  }

  add(region: Region):Observable<Region>{
    return this.http.post<Region>(this.url, region);
  }

  update(region: Region):Observable<Region>{
    return this.http.put<Region>(this.url, region);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
