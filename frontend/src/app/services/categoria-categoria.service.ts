import { CategoriaCategoria } from './../model/categoriaCategoria.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriaCategoriaService {

  private url = environment.baseUrl + '/categoria_categoria';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<CategoriaCategoria[]> {
  return this.http.get<CategoriaCategoria[]>(this.url);
  }

  get(id: number):Observable<CategoriaCategoria> {
    return this.http.get<CategoriaCategoria>(this.url + '?id=' + id);
  }

  add(categoriaCategoria: CategoriaCategoria):Observable<CategoriaCategoria>{
    return this.http.post<CategoriaCategoria>(this.url, categoriaCategoria);
  }

  update(categoriaCategoria: CategoriaCategoria):Observable<CategoriaCategoria>{
    return this.http.put<CategoriaCategoria>(this.url, categoriaCategoria);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
