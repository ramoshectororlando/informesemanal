import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router
    ) { }

    logoutUser(){
      sessionStorage.removeItem('usuarioo')
      this.router.navigate(['/login'])
      
    }
  
    loggedIn(){
      return !!sessionStorage.getItem('usuarioo')
    }
  
    getUser(){
      return sessionStorage.getItem('usuarioo')
    }
  
}
