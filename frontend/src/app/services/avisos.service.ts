import { Avisos } from './../model/avisos.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AvisosService {

  private url = environment.baseUrl + '/avisos';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Avisos[]> {
    return this.http.get<Avisos[]>(this.url);
    }
  
    get(id: number):Observable<Avisos> {
      return this.http.get<Avisos>(this.url + '?id=' + id);
    }
  
    add(avisos: Avisos):Observable<Avisos>{
      return this.http.post<Avisos>(this.url, avisos);
    }
  
    update(avisos: Avisos):Observable<Avisos>{
      return this.http.put<Avisos>(this.url, avisos);
    }
  
    delete(id: number){
      return this.http.delete(this.url + '/' + id);
    }
}
