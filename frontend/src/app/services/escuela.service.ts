import { Escuela } from './../model/escuela.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EscuelaService {

  private url = environment.baseUrl + '/escuela';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Escuela[]> {
  return this.http.get<Escuela[]>(this.url);
  }

  get(id: number):Observable<Escuela> {
    return this.http.get<Escuela>(this.url + '?id=' + id);
  }

  add(escuela: Escuela):Observable<Escuela>{
    return this.http.post<Escuela>(this.url, escuela);
  }

  update(escuela: Escuela):Observable<Escuela>{
    return this.http.put<Escuela>(this.url, escuela);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
