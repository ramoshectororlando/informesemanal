import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ValidacionService {

  datoValidacion: string;

  constructor(
    private toastr: ToastrService,
    private router: Router,
  ) { }

  saveDato( valor: string){
    this.datoValidacion = valor;
  }

  getDato(){
    return this.datoValidacion;
  }

}
