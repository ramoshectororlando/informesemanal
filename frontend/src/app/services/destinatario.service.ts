import { Destinatario } from './../model/destinatario.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DestinatarioService {

  private url = environment.baseUrl + '/destinatario';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Destinatario[]> {
  return this.http.get<Destinatario[]>(this.url);
  }

  get(id: number):Observable<Destinatario> {
    return this.http.get<Destinatario>(this.url + '?id=' + id);
  }

  add(destinatario: Destinatario):Observable<Destinatario>{
    return this.http.post<Destinatario>(this.url, destinatario);
  }

  update(destinatario: Destinatario):Observable<Destinatario>{
    return this.http.put<Destinatario>(this.url, destinatario);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
