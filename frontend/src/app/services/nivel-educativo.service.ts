import { environment } from './../../environments/environment';
import { NivelEducativo } from './../model/nivelEducativo.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NivelEducativoService {

  private url = environment.baseUrl + '/nivel_educativo';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<NivelEducativo[]> {
  return this.http.get<NivelEducativo[]>(this.url);
  }

  get(id: number):Observable<NivelEducativo> {
    return this.http.get<NivelEducativo>(this.url + '?id=' + id);
  }

  add(nivelEducativo: NivelEducativo):Observable<NivelEducativo>{
    return this.http.post<NivelEducativo>(this.url, nivelEducativo);
  }

  update(nivelEducativo: NivelEducativo):Observable<NivelEducativo>{
    return this.http.put<NivelEducativo>(this.url, nivelEducativo);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
