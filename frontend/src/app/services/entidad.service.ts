import { environment } from './../../environments/environment';
import { Entidad } from './../model/entidad.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EntidadService {

  private url = environment.baseUrl + '/entidad';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Entidad[]> {
  return this.http.get<Entidad[]>(this.url);
  }

  get(id: number):Observable<Entidad> {
    return this.http.get<Entidad>(this.url + '?id=' + id);
  }

  add(entidad: Entidad):Observable<Entidad>{
    return this.http.post<Entidad>(this.url, entidad);
  }

  update(entidad: Entidad):Observable<Entidad>{
    return this.http.put<Entidad>(this.url, entidad);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
