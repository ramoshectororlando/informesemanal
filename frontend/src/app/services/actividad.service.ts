import { Actividad } from './../model/actividad.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ActividadService {

  private url = environment.baseUrl + '/actividad';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Actividad[]> {
  return this.http.get<Actividad[]>(this.url);
  }

  get(id: number):Observable<Actividad> {
    return this.http.get<Actividad>(this.url + '?id=' + id);
  }

  add(actividad: Actividad):Observable<Actividad>{
    return this.http.post<Actividad>(this.url, actividad);
  }

  update(actividad: Actividad):Observable<Actividad>{
    return this.http.put<Actividad>(this.url, actividad);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }

  getAllByPersonalId(cuil: string): Observable<Actividad[]>{
    return this.http.get<Actividad[]>(this.url + "/cuil?cuil=" +cuil);
  }
}
