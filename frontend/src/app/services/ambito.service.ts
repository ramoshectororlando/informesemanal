import { environment } from './../../environments/environment';
import { Ambito } from './../model/ambito.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AmbitoService {

  private url = environment.baseUrl + '/ambito';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Ambito[]> {
  return this.http.get<Ambito[]>(this.url);
  }

  get(id: number):Observable<Ambito> {
    return this.http.get<Ambito>(this.url + '?id=' + id);
  }

  add(ambito: Ambito):Observable<Ambito>{
    return this.http.post<Ambito>(this.url, ambito);
  }

  update(ambito: Ambito):Observable<Ambito>{
    return this.http.put<Ambito>(this.url, ambito);
  }

  delete(id: number){
    return this.http.delete(this.url + '/' + id);
  }
}
