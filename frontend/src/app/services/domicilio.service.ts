import { environment } from './../../environments/environment';
import { Domicilio } from './../model/domicilio.model';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DomicilioService {

  private url = environment.baseUrl + '/domicilio';
  private httpHeaders = new HttpHeaders({'Content-Type':'application/json'});

  constructor(private http: HttpClient) { }

  getAll():Observable<Domicilio[]> {
    return this.http.get<Domicilio[]>(this.url);
    }
  
    get(id: number):Observable<Domicilio> {
      return this.http.get<Domicilio>(this.url + '?id=' + id);
    }
  
    add(domicilio: Domicilio):Observable<Domicilio>{
      return this.http.post<Domicilio>(this.url, domicilio);
    }
  
    update(domicilio: Domicilio):Observable<Domicilio>{
      return this.http.put<Domicilio>(this.url, domicilio);
    }
  
    delete(id: number){
      return this.http.delete(this.url + '/' + id);
    }

}
