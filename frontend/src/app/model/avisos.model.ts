export class Avisos{
    id: number;
    titulo: string;
    mensaje: string;
    fecha: string;
    eliminado: boolean;
    leido: boolean;
}