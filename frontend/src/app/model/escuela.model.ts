import { Persona } from './persona.model';
import { Ambito } from './ambito.model';
import { Sector } from './sector.model';
import { NivelEducativo } from './nivelEducativo.model';
import { Region } from './region.model';
import { Domicilio } from './domicilio.model';
import { Personal } from './personal.model';

export class Escuela{
    id: number;
    cue: string;
    nombre: string;
    telefono: string;
    email: string;
    domicilio: Domicilio;
    region: Region;
    nivelEducativo: NivelEducativo;
    sector: Sector;
    ambito: Ambito;
    persona: Persona;
    personal: Personal = new Personal();
}