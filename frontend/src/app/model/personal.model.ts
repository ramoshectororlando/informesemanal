import { Escuela } from 'src/app/model/escuela.model';
import { Entidad } from './entidad.model';
import { Persona } from './persona.model';

export class Personal{
    id: number;
    funcion: string;
    persona: Persona = new Persona();
    entidad: Entidad = new Entidad();
    escuelas: Escuela[]=[];
}