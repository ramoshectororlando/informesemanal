export class Domicilio{
    id: number;
    calle: string;
    numero: number;
    barrio: string;
    departamento: string;
    codigoPostal: number;
}