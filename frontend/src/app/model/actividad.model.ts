import { Destinatario } from './destinatario.model';
import { Categoria } from './categoria.model';
import { Entidad } from './entidad.model';
import { Escuela } from './escuela.model';
import { Personal } from './personal.model';

export class Actividad{
    id: number;
    fecha: string;
    hora: string;
    detalle: string;
    cantidadParticipantes: number;
    personal: Personal;
    escuela: Escuela;
    categoria: Categoria;
    destinatario: Destinatario;
    duracionHora: number;
    duracionMinuto: number;
    activo: boolean;
    fechaCarga: string;
}