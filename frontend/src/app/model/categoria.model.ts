import { CategoriaCategoria } from './categoriaCategoria.model';

export class Categoria{
    id: number;
    categoria: string;
    categoriaCategoria: CategoriaCategoria;
}