import { Escuela } from './escuela.model';
import { Domicilio } from './domicilio.model';

export class Persona{
    id: number;
    cuil: string;
    apellido: string;
    nombres: string;
    telefono: string;
    email: string;
    domicilio: Domicilio = new Domicilio();
    escuelas: Escuela[];
}